#version 120

uniform sampler2D texture;
varying vec2 vTexCoord;
varying vec4 vColor;

void main() {
	vec4 color = vColor * texture2D(texture, vTexCoord);
	if(color.a < 0.1) {
		discard;
	}
	gl_FragColor = color;
}