#version 120

uniform sampler2D texture;
varying vec2 vTexCoord;
varying float vAO;

void main() {
	gl_FragColor = vec4(vec3(vAO), 1.0) * texture2D(texture, vTexCoord);
}