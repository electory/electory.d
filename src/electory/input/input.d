module electory.input.input;

import electory.client.game;
import electory.entity.player.player;
import electory.entity.player.playercontrollerclient;
import derelict.glfw3;
import std.math : PI;
import std.stdio;

struct KeyboardEvent {
    int key;
    int scancode;
    int action;
    int modifiers;
}

struct MouseEvent {
    double x;
    double y;
    double dx;
    double dy;
    int button;
    int action;
    int modifiers;
}

class InputHandler {
    private:
    bool[int] keyStates;

    double lastX = 0.0;
    double lastY = 0.0;
    public:
    Electory tc;

    this(Electory tc) {
        this.tc = tc;
    }

    void handleKeyboardEvent(KeyboardEvent event) nothrow {
        try {
            keyStates[event.key] = event.action != GLFW_RELEASE;
            if(tc.world && tc.world.currentPlayer/* && !tc.guiHandler.blocksWorldInput*/) {
            	if(event.key == GLFW_KEY_F2) {
            		tc.world.save();
            	}
            }
            // tc.guiHandler.processKeyboardEvent(event);
        } catch(Exception e) {

        }
    }

    void handleKeyboardCharEvent(uint c) nothrow {
        try {
            //tc.guiHandler.processKeyboardCharEvent(c);
        } catch(Exception e) {

        }
    }

    bool isKeyPressed(int keycode) nothrow {
        return keycode in keyStates && keyStates[keycode];
    }

    void handleMouseMoveEvent(double x, double y) nothrow {
        MouseEvent event = void;
        event.x = x;
        event.y = y;
        event.dx = x - lastX;
        event.dy = y - lastY;
        event.button = -1;
        event.action = -1;
        event.modifiers = -1;

        lastX = x;
        lastY = y;

        handleMouseEvent(event);
    }

    void handleMouseEvent(MouseEvent event) nothrow {
        try {
            if(tc.world && tc.world.currentPlayer/* && !tc.guiHandler.blocksWorldInput*/) {
                if(auto pcc = cast(IPlayerControllerClient) tc.world.currentPlayer.playerController) {
                    pcc.processMouseEvent(event, tc);
                }
            }
            // tc.guiHandler.processMouseEvent(event);
        } catch(Exception e) {

        }
    }

    void handleMouseButtonEvent(int button, int action, int modifiers) nothrow {
        MouseEvent event = void;
        event.x = lastX;
        event.y = lastY;
        event.dx = event.dy = 0.0;
        event.button = button;
        event.action = action;
        event.modifiers = modifiers;

        handleMouseEvent(event);
    }

    void initCursor(double x, double y) nothrow @nogc {
        lastX = x;
        lastY = y;
    }
}