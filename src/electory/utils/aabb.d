module electory.utils.aabb;

import electory.utils.enums;
import gfm.math.box;
import gfm.math.vector;
import gfm.math.shapes;

import std.stdio;
import std.math;
import std.algorithm.comparison;

T clipAndCollide(EnumAxis axis, T, int N)(const ref Box!(T, N) b, const ref Box!(T, N) c, T a) {
    static if(axis == EnumAxis.AXIS_X) {
        T max;
        if (c.max.y <= b.min.y)
            return a;
        if (c.min.y >= b.max.y) {
            return a;
        }
        static if(N > 2) {
            if (c.max.z <= b.min.z)
                return a;
            if (c.min.z >= b.max.z) {
                return a;
            }
        }
        if (a > 0.0f && c.max.x <= b.min.x && (max = b.min.x - c.max.x - T.epsilon) < a) {
            a = max;
        }
        if (a >= 0.0f)
            return a;
        if (c.min.x < b.max.x)
            return a;
        max = b.max.x - c.min.x + T.epsilon;
        if (max <= a)
            return a;
        return max;
    } else static if(axis == EnumAxis.AXIS_Y) {
        T max;
        if (c.max.x <= b.min.x)
            return a;
        if (c.min.x >= b.max.x) {
            return a;
        }
        static if(N > 2) {
            if (c.max.z <= b.min.z)
                return a;
            if (c.min.z >= b.max.z) {
                return a;
            }
        }
        if (a > 0.0f && c.max.y <= b.min.y && (max = b.min.y - c.max.y - T.epsilon) < a) {
            a = max;
        }
        if (a >= 0.0f)
            return a;
        if (c.min.y < b.max.y)
            return a;
        max = b.max.y - c.min.y + T.epsilon;
        if (max <= a)
            return a;
        return max;
    } else static if(N > 2 && axis == EnumAxis.AXIS_Z) {
        T max;
        if (c.max.x <= b.min.x)
            return a;
        if (c.min.x >= b.max.x) {
            return a;
        }
        if (c.max.y <= b.min.y)
            return a;
        if (c.min.y >= b.max.y) {
            return a;
        }
        if (a > 0.0f && c.max.z <= b.min.z && (max = b.min.z - c.max.z - T.epsilon) < a) {
            a = max;
        }
        if (a >= 0.0f)
            return a;
        if (c.min.z < b.max.z)
            return a;
        max = b.max.z - c.min.z + T.epsilon;
        if (max <= a)
            return a;
        return max;
    } else {
        static assert(false);
    }
}

Box!(T, N) expandBox(T, int N)(Box!(T, N) b, Vector!(T, N) v) {
    Box!(T, N) c = b;
    foreach(i; 0..N) {
        auto a = v[i];

        if(a < 0) {
            c.min[i] += v[i];
        } else if(a > 0) {
            c.max[i] += v[i];
        }
    }
    return c;
}

struct AABBIntersectionResult(T) {
    bool hasHit;
    T distance;
    EnumSide side;
}

AABBIntersectionResult!T intersectAABB(T)(Ray!(T, 3) ray, Box!(T, 3) aabb) {
    auto dirfrac = 1.0f / ray.dir;

    T t1 = (aabb.min.x - ray.orig.x) * dirfrac.x;
    T t2 = (aabb.max.x - ray.orig.x) * dirfrac.x;
    T t3 = (aabb.min.y - ray.orig.y) * dirfrac.y;
    T t4 = (aabb.max.y - ray.orig.y) * dirfrac.y;
    T t5 = (aabb.min.z - ray.orig.z) * dirfrac.z;
    T t6 = (aabb.max.z - ray.orig.z) * dirfrac.z;

    T tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
    T tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

    if(tmax < 0 || tmin > tmax) {
        return AABBIntersectionResult!T(false, tmax, EnumSide.SIDE_UNKNOWN);
    }

    EnumSide side = EnumSide.SIDE_UNKNOWN;
    if(tmin == t1) {
        side = EnumSide.SIDE_SOUTH;
    } else if(tmin == t2) {
        side = EnumSide.SIDE_NORTH;
    } else if(tmin == t3) {
        side = EnumSide.SIDE_WEST;
    } else if(tmin == t4) {
        side = EnumSide.SIDE_EAST;
    } else if(tmin == t5) {
        side = EnumSide.SIDE_BOTTOM;
    } else if(tmin == t6) {
        side = EnumSide.SIDE_TOP;
    }

    return AABBIntersectionResult!T(true, tmin, side);
}