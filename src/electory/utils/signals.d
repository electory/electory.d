module electory.utils.signals;

mixin template ESignal(T1...) {
    void delegate(T1)[] delegateSlots;
    void function(T1)[] functionSlots;

    final void connect(void delegate(T1) slot) {
        delegateSlots ~= slot;
    }

    final void connect(void function(T1) slot) {
        functionSlots ~= slot;
    }

    final void emit(T1 args) {
        foreach(del; delegateSlots) del(args);
        foreach(fun; functionSlots) fun(args);
    }
}