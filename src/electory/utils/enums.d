module electory.utils.enums;

import gfm.math.vector;

public:

enum EnumAxis : int {
    AXIS_X = 0,
    AXIS_Y = 1,
    AXIS_Z = 2
}

enum EnumSide : int {
	SIDE_TOP,
	SIDE_BOTTOM,
	SIDE_NORTH,
	SIDE_SOUTH,
	SIDE_WEST,
	SIDE_EAST,
	SIDE_UNKNOWN = -1
}

vec3!T toVector(T)(EnumSide side) {
	switch(side) {
		case EnumSide.SIDE_TOP:
			return vec3!T(0, 0, 1);
		case EnumSide.SIDE_BOTTOM:
			return vec3!T(0, 0, -1);
		case EnumSide.SIDE_NORTH:
			return vec3!T(1, 0, 0);
		case EnumSide.SIDE_SOUTH:
			return vec3!T(-1, 0, 0);
		case EnumSide.SIDE_WEST:
			return vec3!T(0, -1, 0);
		case EnumSide.SIDE_EAST:
			return vec3!T(0, 1, 0);
		default:
			return vec3!T(0, 0, 0);
	}
}