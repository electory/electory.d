module electory.utils.gameconfig;

class GameConfig {
	public:
	float fov = 90.0f;
	float mouseSensitivity = 0.5f;
	bool invertMouseY = false;
	bool displayFPS = false;
	bool showInGameStats = false;
}
