module electory.game.game;

import electory.resource.manager;
import electory.block.block;
import electory.block.construction_registry;
import electory.block.grass;
import electory.block.water;
import electory.utils.gameconfig;
import electory.entity.registry;
import electory.entity.player.player;

abstract class ElectoryGame {
    public:
    ResourceManager resourceManager;
    BlockRegistry blockRegistry;
    EntityRegistry entityRegistry;
    GameConfig gameConfig;

    void preInit() {
        resourceManager = new ResourceManager();
        blockRegistry = new BlockRegistry(resourceManager);
        entityRegistry = new EntityRegistry(resourceManager);
        gameConfig = new GameConfig;
    }

    void init() {
        registeredBlockClasses[typeid(Block)] = new BlockConstructionInfoImpl!Block;
        registeredBlockClasses[typeid(BlockGrass)] = new BlockConstructionInfoImpl!BlockGrass;
        registeredBlockClasses[typeid(BlockWater)] = new BlockConstructionInfoImpl!BlockWater;
        {
            Block blockCobblestone = new Block(ResourceLocation("base:cobblestone"), "Cobblestone");
            blockCobblestone.spriteLocation = ResourceLocation("textures/terrain/cobblestone.png");
            blockRegistry.registerBlock(blockCobblestone);
        }
        {
            Block blockDirt = new Block(ResourceLocation("dirt"), "Dirt");
            blockDirt.spriteLocation = ResourceLocation("textures/terrain/dirt.png");
            blockRegistry.registerBlock(blockDirt);
        }
        {
            Block blockPlanks = new Block(ResourceLocation("planks"), "Planks");
            blockPlanks.spriteLocation = ResourceLocation("textures/terrain/planks.png");
            blockRegistry.registerBlock(blockPlanks);
        }
        {
            Block blockLeaves = new Block(ResourceLocation("leaves"), "Leaves");
            blockLeaves.spriteLocation = ResourceLocation("textures/terrain/leaves.png");
            blockRegistry.registerBlock(blockLeaves);
        }
        {
            Block blockGravel = new Block(ResourceLocation("gravel"), "Gravel");
            blockGravel.spriteLocation = ResourceLocation("textures/terrain/gravel.png");
            blockRegistry.registerBlock(blockGravel);
        }
        {
            Block blockGrass = new BlockGrass(ResourceLocation("grass"));
            blockRegistry.registerBlock(blockGrass);
        }
        {
            Block blockWater = new BlockWater(ResourceLocation("water"));
            blockRegistry.registerBlock(blockWater);
        }
        {
            Block blockAODebug = new Block(ResourceLocation("ao_debug"), "AO DEBUG BLOCK");
            blockAODebug.spriteLocation = ResourceLocation("textures/terrain/ao_debug.png");
            blockRegistry.registerBlock(blockAODebug);
        }

        // Entity reg

        {
        	entityRegistry.register!EntityPlayer(ResourceLocation("base:player/player"));
        }
    }

    abstract string getUserDataFolder();
}