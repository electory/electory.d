module electory.world.chunk;

enum ChunkFlags {
    NEEDS_RENDER_UPDATE = 1
}

class Chunk {
    public:
    ushort[16][16][16] blockData;

    int chunkX;
    int chunkY;
    int chunkZ;

    ChunkFlags flags = ChunkFlags.NEEDS_RENDER_UPDATE;

    this(int chunkX, int chunkY, int chunkZ) {
        this.chunkX = chunkX;
        this.chunkY = chunkY;
        this.chunkZ = chunkZ;
    }

    this(Chunk chunk) {
        this.chunkX = chunk.chunkX;
        this.chunkY = chunk.chunkY;
        this.chunkZ = chunk.chunkZ;
        this.blockData = chunk.blockData;
    }

    int opDollar(size_t pos)() pure immutable {
    	return 16;
    }

    /**
     * Retrieve block ID at coordinates
     */
    ushort opIndex(size_t x, size_t y, size_t z) pure const {
        assert(x >= 0 && x < 16 && y >= 0 && y < 16 && z >= 0 && z < 16);

        return blockData[x][y][z];
    }

    /**
     * Set block ID at coordinates
     */
    void opIndexAssign(ushort blockId, int x, int y, int z) {
        assert(x >= 0 && x < 16 && y >= 0 && y < 16 && z >= 0 && z < 16);

        blockData[x][y][z] = blockId;
    }
}