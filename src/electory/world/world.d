module electory.world.world;

import std.typecons;
import containers.hashset;
import gfm.math;
import std.math;
import std.stdio;
import std.container;
import std.file;
import std.conv;
import std.path;
import std.array;
import std.random;
import std.system;
import std.algorithm;
import toml;
import sel.nbt;

import electory.world.chunk;
import electory.world.chunkprovider;
import electory.entity.entity;
import electory.entity.player.player;
import electory.game.game;
import electory.block.block;
import electory.resource.manager;

interface IBlockAccess {
	inout(Block) getBlockAt(int x, int y, int z) inout;
	void setBlockAt(int x, int y, int z, Block block);
	Chunk getChunkAt(int x, int y, int z);
}

class World : IBlockAccess {
	private:
    void markBlockForRenderUpdateLocally(int x, int y, int z) {
		/*if(auto tc = cast(IClient)tc) {
			tc.getRenderer().worldRenderer.markChunkForRenderUpdate(x >> 4, y >> 4, z >> 4);
		}*/
		getChunkAt(x >> 4, y >> 4, z >> 4).flags |= ChunkFlags.NEEDS_RENDER_UPDATE;
    }

    public:
    Entity[] entities;
    EntityPlayer currentPlayer;
    ElectoryGame tc;
    auto blockIdRegistry = new BlockIDRegistry;
    ChunkProviderSP chunkProvider;
    vec3i renderCenter = vec3i(0, 0, 0);

    int seed;

    this(ElectoryGame game) {
        this.tc = game;
        this.seed = unpredictableSeed;
    }

    box3f[] getBlockAABBsWithinAABB(box3f aabb, bool ignorePassable) {
        box3f[] ret;

        foreach(x; (cast(int)floor(aabb.min.x))..(cast(int)ceil(aabb.max.x))) {
            foreach(y; (cast(int)floor(aabb.min.y))..(cast(int)ceil(aabb.max.y))) {
                foreach(z; (cast(int)floor(aabb.min.z))..(cast(int)ceil(aabb.max.z))) {
                    Block block = getBlockAt(x, y, z);
                    if(block !is null && (!ignorePassable || block.isImpassable())) {
                        ret ~= block.getAABB(this, x, y, z);
                    }
                }
            }
        }

        return ret;
    }

	bool isAABBWithinLiquid(box3f aabbIn) {
		int x1 = cast(int) floor(aabbIn.min.x);
		int y1 = cast(int) floor(aabbIn.min.y);
		int z1 = cast(int) floor(aabbIn.min.z);
		int x2 = cast(int) ceil(aabbIn.max.x);
		int y2 = cast(int) ceil(aabbIn.max.y);
		int z2 = cast(int) ceil(aabbIn.max.z);
		foreach (x; x1..x2) {
			foreach (y; y1..y2) {
				foreach (z; z1..z2) {
					Block block = getBlockAt(x, y, z);
					if (block && block.isLiquid()) {
						return true;
					}
				}
			}
		}

		return false;
	}

    void markBlockForRenderUpdate(int x, int y, int z) {
    	foreach(ux; x-1..x+2) {
	    	foreach(uy; y-1..y+2) {
		    	foreach(uz; z-1..z+2) {
		    		markBlockForRenderUpdateLocally(ux, uy, uz);
		    	}
	    	}
    	}
    }

    inout(Block) getBlockAt(int x, int y, int z) inout {
        if(auto chunk = chunkProvider.provideChunk(x >> 4, y >> 4, z >> 4)) {
            return blockIdRegistry.getBlock(chunk[x & 15, y & 15, z & 15]);
        }
        return null;
    }

    void setBlockAt(int x, int y, int z, Block block) {
        if(auto chunk = chunkProvider.provideChunk(x >> 4, y >> 4, z >> 4)) {
            chunk[x & 15, y & 15, z & 15] = blockIdRegistry.getId(block);
			markBlockForRenderUpdate(x, y, z);
        }
    }

    Chunk getChunkAt(int x, int y, int z) {
    	return chunkProvider.provideChunk(x, y, z);
    }

    void chunkLoadingTick() {
    	int sx = (cast(int)(currentPlayer.pos.x) >> 4) - 8;
    	int sy = (cast(int)(currentPlayer.pos.y) >> 4) - 8;
    	int sz = (cast(int)(currentPlayer.pos.z) >> 4) - 8;

    	chunkProvider.clearKeepAliveFlags();
    	foreach(cx; sx..sx+16) {
	    	foreach(cy; sy..sy+16) {
		    	foreach(cz; sz..sz+16) {
		    		chunkProvider.loadChunk(cx, cy, cz);
		    	}
	    	}
    	}
    	chunkProvider.removeDeadChunks();

		renderCenter = vec3i(sx + 8, sy + 8, sz + 8);
    }

	void addEntity(Entity entity) {
		entities ~= entity;
	}

    void init() {
    	chunkProvider = new ChunkProviderSP(this, new ChunkProviderGenerate(this));
        addEntity(currentPlayer = new EntityPlayer(this));
        currentPlayer.setPosition(vec3f(128.0, 128.0, 128.0));
    }

    void update() {
    	chunkLoadingTick();
		entities = entities.filter!(e=>!e.removeScheduled).array;
        foreach(entity; entities) {
            entity.update();
            entity.postUpdate();
        }
    }

    void load() {
    	auto utomlPath = getWorldPath().chainPath("universe.toml");
    	if(utomlPath.exists) {
	    	TOMLDocument doc = parseTOML(std.file.readText(utomlPath));

	    	seed = cast(int) doc["seed"].integer;
	    	blockIdRegistry.readFromTOML(tc.blockRegistry, doc["blockIdRegistry"]);
    	}

    	auto entitiesPath = getWorldPath().chainPath("entities.nbt");

    	bool playerSpawnedFromNBT = false;

    	if(entitiesPath.exists) {
	    	auto cs = new ClassicStream!(Endian.bigEndian)(cast(ubyte[])std.file.read(entitiesPath));

	    	entities = [];

	    	Compound tag = cast(Compound)cs.readTag();

	    	auto tagList = cast(List)tag["entities"];

	    	foreach(entityTag_; tagList) {
	    		auto entityTag = cast(Compound)entityTag_;

	    		Entity entity = tc.entityRegistry[ResourceLocation(cast(String)entityTag["_entityType"])].instantiate(this);

	    		entity.readFromNBT(entityTag);

	    		addEntity(entity);

	    		if(auto playerToSpawn = cast(EntityPlayer)entity) {
	    			playerSpawnedFromNBT = true;
	    			currentPlayer = playerToSpawn;
	    		}
	    	}
    	}
    }

    void save() {
    	{
	    	TOMLDocument doc;
	    	doc["seed"] = seed;
	    	doc["blockIdRegistry"] = blockIdRegistry.writeToTOML();

	    	std.file.write(getWorldPath().chainPath("universe.toml"), to!string(doc));
    	}

    	{
	    	auto tag = new Compound;
	    	auto tagList = new List;
	    	foreach(entity; entities) {
				if(entity.isTransient) {
					continue;
				}
	    		auto entityTag = entity.writeToNBT();
	    		entityTag["_entityType"] = tc.entityRegistry[typeid(entity)].toString();
	    		tagList ~= entityTag;
	    	}
	    	tag["entities"] = tagList;

	    	auto cs = new ClassicStream!(Endian.bigEndian);
	    	cs.writeTag(tag);

	    	std.file.write(getWorldPath().chainPath("entities.nbt"), cs.data);
    	}

    	foreach(chunk; chunkProvider.getAllLoadedChunks) {
    		chunkProvider.saveChunk(chunk);
    	}
    }

    void unloadSafe() {
    	chunkProvider.clearKeepAliveFlags();
    	chunkProvider.removeDeadChunks();
    	save();
    }

    auto getWorldPath() {
    	auto path = chainPath(tc.getUserDataFolder(), "universe").array;
    	path.mkdirRecurse();
    	return path;
    }

	ref auto getEntities() {
		return entities;
	}
}

class ChunkCache : IBlockAccess {
	private:
	immutable int startX;
	immutable int startY;
	immutable int startZ;
	immutable int endX;
	immutable int endY;
	immutable int endZ;
	BlockIDRegistry blockIdRegistry;
	Chunk[][][] chunkArray;

	static Chunk[][][] buildChunkArray(World world, int startX, int startY, int startZ, int endX, int endY, int endZ, RedBlackTree!ChunkPos chunkPositions) {
		Chunk[][][] chunkArr = new Chunk[][][](endX - startX, endY - startY, endZ - startZ);
        
		foreach(chunkPos; chunkPositions) {
			auto wc = world.getChunkAt(chunkPos.x, chunkPos.y, chunkPos.z);
			if(wc !is null)	chunkArr[chunkPos.x - startX][chunkPos.y - startY][chunkPos.z - startZ] = new Chunk(wc);
		}

		return chunkArr;
	}

	public:
	@disable this();

	this(World world, RedBlackTree!ChunkPos chunkPositions) immutable {
		int sx = int.max;
		int sy = int.max;
		int sz = int.max;
		int ex = int.min;
		int ey = int.min;
		int ez = int.min;

		foreach(chunkPos; chunkPositions) {
			if(chunkPos.x < sx) sx = chunkPos.x;
			if(chunkPos.y < sy) sy = chunkPos.y;
			if(chunkPos.z < sz) sz = chunkPos.z;

			if(chunkPos.x + 1 > ex) ex = chunkPos.x + 1;
			if(chunkPos.y + 1 > ey) ey = chunkPos.y + 1;
			if(chunkPos.z + 1 > ez) ez = chunkPos.z + 1;
		}

		if(ex < sx) ex = sx;
		if(ey < sy) ey = sy;
		if(ez < sz) ez = sz;

		this.startX = sx;
		this.startY = sy;
		this.startZ = sz;
		this.endX = ex;
		this.endY = ey;
		this.endZ = ez;

		this.chunkArray = cast(immutable)buildChunkArray(world, startX, startY, startZ, endX, endY, endZ, chunkPositions);

		this.blockIdRegistry = new immutable BlockIDRegistry(world.blockIdRegistry);
	}

	inout(Block) getBlockAt(int x, int y, int z) inout {
		immutable int sx = startX << 4;
		immutable int sy = startY << 4;
		immutable int sz = startZ << 4;
		immutable int ex = endX << 4;
		immutable int ey = endY << 4;
		immutable int ez = endZ << 4;
		auto chunk = chunkArray[(x - sx) >> 4][(y - sy) >> 4][(z - sz) >> 4];
		if(x < sx || y < sy || z < sz || x >= ex || y >= ey || z >= ez || chunk is null) {
			return null;
		}
		return blockIdRegistry.getBlock(chunk[x & 15, y & 15, z & 15]);
	}

	void setBlockAt(int x, int y, int z, Block block) {
		immutable int sx = startX << 4;
		immutable int sy = startY << 4;
		immutable int sz = startZ << 4;
		immutable int ex = endX << 4;
		immutable int ey = endY << 4;
		immutable int ez = endZ << 4;
		Chunk chunk = chunkArray[(x - sx) >> 4][(y - sy) >> 4][(z - sz) >> 4];
		if(x < sx || y < sy || z < sz || x >= ex || y >= ey || z >= ez || chunk is null) {
			return;
		}
		chunk[x & 15, y & 15, z & 15] = blockIdRegistry.getId(block);
	}

    Chunk getChunkAt(int x, int y, int z) {
    	if(x < startX || y < startY || z < startZ || x >= endX || y >= endY || z >= endZ) {
    		return null;
    	}
    	return chunkArray[x - startX][y - startY][z - startZ];
    }
}