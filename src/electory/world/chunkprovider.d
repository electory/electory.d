module electory.world.chunkprovider;

import gfm.math.vector;
import noise.mod.perlin;

import std.algorithm.comparison;
import std.range;
import std.stdio;
import std.typecons;
import std.file;
import std.path;
import std.format;
import std.system : Endian;

import sel.nbt;

import electory.world.world;
import electory.world.chunk;
import electory.resource.manager;

enum ChunkLoadState {
	CHUNK_UNLOADED,
	CHUNK_CANNOT_BE_LOADED,
	CHUNK_LOADING,
	CHUNK_LOADED,
	CHUNK_LOADED_KEEPALIVE
}

struct ChunkPos {
	int x, y, z;

	size_t toHash() @trusted nothrow {
		return hashOf(x, hashOf(y, hashOf(z)));
	}

	string toString() pure {
		return format!"%d, %d, %d"(x, y, z);
	}

	int opCmp(ref const ChunkPos c) const {
		return tuple(x, y, z).opCmp(tuple(c.x, c.y, c.z));
	}
}

interface ChunkProvider {
	inout(Chunk) provideChunk(int x, int y, int z) inout;

	ChunkLoadState getChunkLoadState(int x, int y, int z);

	void loadChunk(int x, int y, int z);

	InputRange!Chunk getAllLoadedChunks();

	void clearKeepAliveFlags();

	void removeDeadChunks();

	void saveChunk(Chunk chunk);

	void addProvisionReceiver(ChunkProvisionReceiver receiver);
}

interface ChunkGenerator {
	Chunk provideChunk(int x, int y, int z);
}

interface ChunkProvisionReceiver {
	void onUnloadChunk(Chunk chunk);
}

class ChunkProviderGenerate : ChunkGenerator {
	public:
	World world;

	this(World world) {
		this.world = world;
	}

	override Chunk provideChunk(int cx, int cy, int cz) {
		Chunk chunk = new Chunk(cx, cy, cz);

		int bcz = cz * 16;
		int nbcz = bcz + 16;

        Perlin perlin = new Perlin;
        perlin.SetSeed(world.seed);
        perlin.SetFrequency(1.0f / 64.0f);

        immutable ushort stoneBlockId = world.blockIdRegistry.getId(world.tc.blockRegistry.getBlock(ResourceLocation("base:cobblestone")));
        immutable ushort dirtBlockId = world.blockIdRegistry.getId(world.tc.blockRegistry.getBlock(ResourceLocation("base:dirt")));
        immutable ushort grassBlockId = world.blockIdRegistry.getId(world.tc.blockRegistry.getBlock(ResourceLocation("base:grass")));
		immutable ushort waterBlockId = world.blockIdRegistry.getId(world.tc.blockRegistry.getBlock(ResourceLocation("base:water")));

        foreach(x; 0..16) {
            foreach(y; 0..16) {
                immutable double h = perlin.GetValue(x + cx * 16, y + cy * 16, 0.0f);
                immutable int hh = cast(int) (h * 32.0 + 64.0);
                immutable int chh = hh - bcz;

                foreach(z; 0..min(16, chh - 5)) {
                    chunk.blockData[x][y][z] = stoneBlockId;
                }

                foreach(z; max(chh - 5, 0)..min(16, chh)) {
                    chunk.blockData[x][y][z] = dirtBlockId;
                }

                if(chh >= 0 && chh < 16) chunk.blockData[x][y][chh] = grassBlockId;

				foreach(z; max(chh, 0)..min(64 - bcz, 16)) {
					chunk.blockData[x][y][z] = waterBlockId;
				}
            }
        }

        return chunk;
	}
}

class ChunkProviderSP : ChunkProvider {
	private:
	Chunk[ChunkPos] loadedChunks;
	ChunkLoadState[ChunkPos] loadedChunkStates;
	ChunkProvisionReceiver[] chunkProvisionReceivers;

	public:
	ChunkProviderGenerate genProvider;
	World world;

	this(World world, ChunkProviderGenerate genProvider) {
		this.world = world;
		this.genProvider = genProvider;
	}

	override inout(Chunk) provideChunk(int x, int y, int z) inout {
		return ChunkPos(x, y, z) in loadedChunks ? loadedChunks[ChunkPos(x, y, z)] : null;
	}

	override ChunkLoadState getChunkLoadState(int x, int y, int z) pure {
		return ChunkPos(x, y, z) in loadedChunkStates ? loadedChunkStates[ChunkPos(x, y, z)] : ChunkLoadState.CHUNK_UNLOADED;
	}

	override void clearKeepAliveFlags() @nogc @safe {
		foreach(ref state; loadedChunkStates) {
			state = ChunkLoadState.CHUNK_LOADED;
		}
	}

	override void removeDeadChunks() {
		foreach(pos; loadedChunks.keys) {
			if(!(pos in loadedChunkStates)) {
				debug {
					assert(false, "Chunk " ~ pos.toString ~ " does not have a provider state. This is not okay!");
				} else {
					loadedChunks[pos].destroy!false;
					loadedChunks.remove(pos);
				}
			} else if(loadedChunkStates[pos] == ChunkLoadState.CHUNK_LOADED) {
				saveChunk(loadedChunks[pos]);

				loadedChunks[pos].destroy!false;
				loadedChunks.remove(pos);
				loadedChunkStates.remove(pos);
			}
		}
	}

	override void loadChunk(int x, int y, int z) {
		if(getChunkLoadState(x, y, z) != ChunkLoadState.CHUNK_UNLOADED) {
			if(getChunkLoadState(x, y, z) == ChunkLoadState.CHUNK_LOADED) {
				loadedChunkStates[ChunkPos(x, y, z)] = ChunkLoadState.CHUNK_LOADED_KEEPALIVE;
			}
			return;
		}

		auto chunksDir = world.getWorldPath().chainPath("chunks");
		auto chunkPath = chunksDir.chainPath(format!"c%d_%d_%d.c3d"(x, y, z));

		Chunk chunk;

		if(chunkPath.exists) {
			chunk = new Chunk(x, y, z);

			auto f = File(chunkPath, "rb");
			int versionNumber = 0;
			f.rawRead((&versionNumber)[0..1]);
			f.rawRead(cast(short[])chunk.blockData);
		} else {
			chunk = genProvider.provideChunk(x, y, z);
		}

		loadedChunks[ChunkPos(x, y, z)] = chunk;

		loadedChunkStates[ChunkPos(x, y, z)] = ChunkLoadState.CHUNK_LOADED_KEEPALIVE;
	}

	override void saveChunk(Chunk chunk) {
		auto chunksDir = world.getWorldPath().chainPath("chunks");

		chunksDir.array.mkdirRecurse();

		auto f = File(chunksDir.chainPath(format!"c%d_%d_%d.c3d"(chunk.chunkX, chunk.chunkY, chunk.chunkZ)), "wb");
		int versionNumber = 0;
		f.rawWrite((&versionNumber)[0..1]);
		f.rawWrite(cast(short[])chunk.blockData);
	}

	override InputRange!Chunk getAllLoadedChunks() {
		return inputRangeObject(loadedChunks.values);
	}

	override void addProvisionReceiver(ChunkProvisionReceiver receiver) {
		chunkProvisionReceivers ~= receiver;
	}
}