module electory.block.water;

import electory.block.block;
import electory.resource.manager;
import electory.client.render.block;
import electory.client.render.atlas;
import electory.world.world;
import electory.utils.enums;

class BlockWater : Block {
	public:
	this(ResourceLocation res) {super(res, "Water");}

	this(BlockWater block) immutable {
		super(block);
    }

    override void registerAtlasSprites(AtlasSpriteManager manager) {
        blockSprite = manager.registerSprite(ResourceLocation("textures/terrain/water.png"));
    }

    override bool shouldRenderInPass(int pass) const {
        return pass == RenderPasses.RENDERPASS_WORLD_WATER;
    }

    override bool shouldBeAmbientOccluded() const {
        return false;
    }

    override bool isImpassable() const {
        return false;
    }

    override bool isLiquid() const {
        return true;
    }

    override bool shouldntFaceBeCulled(const IBlockAccess world, int x, int y, int z, int pass, EnumSide side) const {
        immutable vec = side.toVector!int;
        const Block another = world.getBlockAt(x + vec.x, y + vec.y, z + vec.z);
        return another is null;
    }
}