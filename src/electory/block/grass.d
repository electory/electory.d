module electory.block.grass;

import electory.block.block;
import electory.utils.enums;
import electory.resource.manager;
import electory.client.render.atlas;
import electory.block.construction_registry;

/*shared static this() {
    registeredBlockClasses[typeid(BlockGrass)] = new BlockConstructionInfoImpl!BlockGrass;
}*/ // TODO:

class BlockGrass : Block {
	private:
	AtlasSpriteId blockSpriteBottom;
	AtlasSpriteId blockSpriteSide;

	public:
	this(ResourceLocation res) {super(res, "Grass");}

	this(BlockGrass block) immutable {
		super(block);

        this.blockSpriteBottom = block.blockSpriteBottom;
        this.blockSpriteSide = block.blockSpriteSide;
    }

    override void registerAtlasSprites(AtlasSpriteManager manager) {
        blockSprite = manager.registerSprite(ResourceLocation("textures/terrain/grass_top.png"));
        blockSpriteSide = manager.registerSprite(ResourceLocation("textures/terrain/grass_side.png"));
        blockSpriteBottom = manager.registerSprite(ResourceLocation("textures/terrain/dirt.png"));
    }

    override immutable(AtlasSprite) getAtlasSprite(immutable(AtlasSpriteManager) manager, EnumSide side) const {
        switch(side) {
        	case EnumSide.SIDE_BOTTOM:
	        	return manager.getSpriteById(blockSpriteBottom);
        	case EnumSide.SIDE_TOP:
	        	return manager.getSpriteById(blockSprite);
	        default:
		        return manager.getSpriteById(blockSpriteSide);
        }
    }
}