module electory.block.construction_registry;

import electory.block.block;

interface BlockConstructionInfo {
    immutable(Block) constructImmutable(Block parent) shared;
}

class BlockConstructionInfoImpl(T) : BlockConstructionInfo {
    immutable(Block) constructImmutable(Block parent) shared {
        return new immutable T(cast(T)parent);
    }
}

shared static BlockConstructionInfo[TypeInfo] registeredBlockClasses;