module electory.block.block;

import containers.hashmap;
import core.atomic;
import gfm.math.box;
import std.algorithm.iteration;
import std.conv;
import std.format;
import std.range;
import std.typecons;
import toml;
import electory.block.construction_registry;

import electory.resource.manager;
import electory.world.world;
import electory.client.render.atlas;
import electory.utils.enums;
import electory.client.render.block;

class Block {
    protected:
    static shared uint nextVirtualID = 0;
    uint virtualID;
    AtlasSpriteId blockSprite;

    public:
    ResourceLocation spriteLocation = void;
    string name;

    /**
     * Constructs a block.
     * Warning: undefined behavior in non-main thread
     */
    this(ResourceLocation registryLocation, string name = "unknown") {
    	this.registryLocation = registryLocation;
        virtualID = atomicOp!"+="(nextVirtualID, 1) - 1;
        this.name = name;
    }

    this(Block block) immutable {
        this.registryLocation = block.registryLocation;
        this.name = block.name;
        this.virtualID = block.virtualID;
        this.blockSprite = block.blockSprite;
    }

    const ResourceLocation registryLocation;

    box3f getAABB(World world, int x, int y, int z) const {
        return box3f(x, y, z, x + 1, y + 1, z + 1);
    }

    immutable(BlockRenderer) getRenderer() const {
        return cubeRenderer;
    }

    void registerAtlasSprites(AtlasSpriteManager manager) {
        blockSprite = manager.registerSprite(spriteLocation);
    }

    immutable(AtlasSprite) getAtlasSprite(immutable AtlasSpriteManager manager) const {
        return manager.getSpriteById(blockSprite);
    }

    immutable(AtlasSprite) getAtlasSprite(immutable AtlasSpriteManager manager, EnumSide side) const {
        return getAtlasSprite(manager);
    }

    bool shouldRenderInPass(int pass) const {
        return pass == RenderPasses.RENDERPASS_WORLD_BASE;
    }

    bool shouldBeAmbientOccluded() const {
        return true;
    }

    bool isImpassable() const {
        return true;
    }

    bool isLiquid() const {
        return false;
    }

    bool shouldntFaceBeCulled(const IBlockAccess world, int x, int y, int z, int pass, EnumSide side) const {
        immutable vec = side.toVector!int;
        const Block another = world.getBlockAt(x + vec.x, y + vec.y, z + vec.z);
        return another is null || !another.shouldRenderInPass(pass);
    }
}


/*shared static this() {
    registeredBlockClasses[typeid(Block)] = new BlockConstructionInfoImpl!Block;
}*/

class BlockRegistry {
    private:
    Block[ResourceLocation] loadedBlocks;
    ResourceManager resMan;

    public:
    this(ResourceManager resMan) {
        this.resMan = resMan;
    }

    @disable this();

    inout(Block) getBlock(const ResourceLocation res) inout {
        assert(res in loadedBlocks);
        return loadedBlocks[res];
    }

    void registerBlock(Block block) {
        loadedBlocks[block.registryLocation] = block;
    }

    auto getRegisteredBlocks() {
        return loadedBlocks.byValue;
    }
}

class BlockIDRegistry {
    private:
    ushort[uint] blockToIdMap;
    Block[65536] idToBlockMap;
    ushort lastUnusedID = 1;

    public:
    this() {

    }

    this(BlockIDRegistry other) immutable {
        idToBlockMap = other.idToBlockMap[0..$].map!(block => block ? registeredBlockClasses[typeid(block)].constructImmutable(block) : null).array;
        blockToIdMap = cast(immutable)other.blockToIdMap.dup;
    }

    pure inout(Block) getBlock(const ushort id) inout {
        inout(Block) block = idToBlockMap[id];
        if(block is null && id != 0) {
            throw new Error(format!"No block registered with ID %d"(id));
        }
        return block;
    }

    ushort registerBlock(Block block, ushort id) {
        blockToIdMap[block.virtualID] = id;
        idToBlockMap[id] = block;

        return id;
    }

    ushort registerBlock(Block block) {
        return registerBlock(block, lastUnusedID++);
    }

    ushort getId(Block block) {
        if(block is null) return 0;

        uint vid = block.virtualID;

        if(vid in blockToIdMap) {
            return blockToIdMap[vid];
        }

        return registerBlock(block);
    }

    TOMLValue writeToTOML() {
    	TOMLValue[string] table;

    	foreach(i, block; idToBlockMap) {
    		if(block) {
		    	table[block.registryLocation.toString()] = i;
    		}
    	}

    	return TOMLValue(table);
    }

    void readFromTOML(BlockRegistry breg, TOMLValue val) {
    	foreach(regloc, id; val.table) {
    		registerBlock(breg.getBlock(ResourceLocation(regloc)), cast(ushort) id.integer);
            if(id.integer >= lastUnusedID) lastUnusedID = cast(ushort)(id.integer + 1);
    	}
    }
}