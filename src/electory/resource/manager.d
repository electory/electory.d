module electory.resource.manager;

import std.file;
import std.path;
import std.conv;
import std.stdio;
import std.array;
import containers.hashmap;

struct ResourceLocation {
    string domain;
    string path;

    this(in string fullname) {
        auto arr = split(fullname, ":");
        if(arr.length == 1) {
            domain = "base";
            path = arr[0];
        } else if(arr.length == 2) {
            domain = arr[0];
            path = arr[1];
        } else {
            throw new Exception("resource location can contain only zero or one colon");
        }
    }

    string toString() pure nothrow const {
        return domain ~ ":" ~ path;
    }

    bool opEquals(const ref ResourceLocation second) pure nothrow @safe @nogc const {
        return domain == second.domain && path == second.path;
    }

    size_t toHash() nothrow pure @safe @nogc const {
        return hashOf(domain).hashOf(hashOf(path));
    }
}

class ResourceManager {
    public:

    string[] dataSources;

    this() {
        string s = to!string(chainPath(dirName(thisExePath), "assets"));
        dataSources ~= s;
    }

    string getResourcePath(ResourceLocation location) {
        foreach_reverse(source; dataSources) {
            string p1 = to!string(chainPath(source, location.domain, location.path));

            if(exists(p1)) {
                return p1;
            }
        }

        throw new Exception("No resource with location " ~ to!string(location));
    }
}