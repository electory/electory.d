module electory.entity.entity;

import gfm.math.vector;
import gfm.math.box;
import gfm.math.funcs;
import containers.hashset;
import std.stdio;

import electory.world.world;
import electory.utils.aabb;
import electory.utils.enums;
import sel.nbt;

abstract class Entity {
    public:
    vec3f newPos = vec3f(0.0, 0.0, 0.0);
    vec3f pos = vec3f(0.0, 0.0, 0.0);
    vec3f oldPos = vec3f(0.0, 0.0, 0.0);
    vec3f velocity = vec3f(0.0, 0.0, 0.0);

    vec3f size = vec3f(0.75f, 0.75f, 1.75f);

    bool onGround = false;
    bool onCeiling = false;
    bool isUnderwater = false;
    bool noclip = false;

    bool removeScheduled = false;

    World world;

    this(World world) @nogc @safe nothrow {
        this.world = world;
    }

    @disable this();

    void update() {
        bool wasUnderwater = isUnderwater;

        isUnderwater = world.isAABBWithinLiquid(getAABB());

        if (onGround && velocity.z < 0) {
            velocity.z = 0.0f;
        }
        if (onCeiling && velocity.z > 0) {
            velocity.z = 0.0f;
        }
        if (!wasUnderwater && isUnderwater) {
            velocity.y *= 0.05f;
        }


        vec3f affectedVel = getAcceleration();
        velocity += affectedVel;

        moveClipped(velocity);
    }

    bool isTransient() {
        return false;
    }

    void scheduleRemove() {
        removeScheduled = true;
    }

    void postUpdate() {
        oldPos = pos;
        pos = newPos;
    }

    box3f getAABB() const pure nothrow {
        return box3f(newPos.x - size.x / 2, newPos.y - size.y / 2, newPos.z, newPos.x + size.x / 2, newPos.y + size.y / 2, newPos.z + size.z);
    }

    void moveClipped(vec3f ofs) {
        double origZOfs = ofs.z;

        box3f paabb = getAABB();

        if (!noclip) {
            box3f taabb = expandBox(getAABB(), ofs);
            box3f[] blockAABBs = world.getBlockAABBsWithinAABB(taabb, true);

            foreach (const ref aabb; blockAABBs) {
                ofs.z = clipAndCollide!(EnumAxis.AXIS_Z)(aabb, paabb, ofs.z);
            }

            paabb = paabb.translate(vec3f(0, 0, ofs.z));

            foreach (const ref aabb; blockAABBs) {
                ofs.x = clipAndCollide!(EnumAxis.AXIS_X)(aabb, paabb, ofs.x);
            }

            paabb = paabb.translate(vec3f(ofs.x, 0, 0));

            foreach (const ref aabb; blockAABBs) {
                ofs.y = clipAndCollide!(EnumAxis.AXIS_Y)(aabb, paabb, ofs.y);
            }

            paabb = paabb.translate(vec3f(0, ofs.y, 0));
        } else {
            paabb = paabb.translate(ofs);
        }

        if (origZOfs < -0.05f) {
            // Passive ground collision check
            onGround = ofs.z > origZOfs;
            onCeiling = false;
        } else if (origZOfs > 0.05f) {
            // Passive ceiling collision check
            onGround = false;
            onCeiling = ofs.z < origZOfs;
        } else {
            // Active ground collision check
            const box3f gaabb = expandBox(paabb, vec3f(0.0f, 0.0f, -0.01f));
            double gofs = -0.005;
            box3f[] gAABBs = world.getBlockAABBsWithinAABB(gaabb, true);

            onGround = false;

            foreach (const ref aabb; gAABBs) {
                gofs = clipAndCollide!(EnumAxis.AXIS_Z)(aabb, paabb, gofs);
                if (gofs >= -0.0025f) {
                    onGround = true;
                    onCeiling = false;
                }
            }
        }

        newPos += ofs;

    }

    vec3f getInterpolatedPosition(float renderPartialTicks) {
        return lerp(oldPos, pos, renderPartialTicks);
    }

    void setPosition(vec3f pos, bool interpolate = false) {
        if (interpolate) {
            newPos = pos;
        } else {
            this.pos = this.newPos = this.oldPos = pos;
        }
    }

    bool hasGravity() const {
        return true;
    }

    vec3f getAcceleration() const {
        return vec3f(0.0f, 0.0f, (hasGravity && !onGround) ? (isUnderwater ? -0.003f : -0.0981f) : 0.0f);
    }

    Compound writeToNBT() const {
    	auto tag = new Compound;
    	tag["x"] = new Float(pos.x);
    	tag["y"] = new Float(pos.y);
    	tag["z"] = new Float(pos.z);
    	tag["velx"] = new Float(velocity.x);
    	tag["vely"] = new Float(velocity.y);
    	tag["velz"] = new Float(velocity.z);
    	tag["ground"] = cast(byte)onGround;
    	tag["ceiling"] = cast(byte)onCeiling;
    	tag["underwater"] = cast(byte)isUnderwater;
    	return tag;
    }

    void readFromNBT(Compound tag) {
    	setPosition(vec3f(cast(Float)tag["x"], cast(Float)tag["y"], cast(Float)tag["z"]), false);
    	velocity = vec3f(cast(Float)tag["velx"], cast(Float)tag["vely"], cast(Float)tag["velz"]);
    	onGround = cast(bool)(cast(Byte)tag["ground"]).value;
    	onCeiling = cast(bool)(cast(Byte)tag["ceiling"]).value;
        isUnderwater = cast(bool)(cast(Byte)tag["underwater"]).value;
    }
}

abstract class EntityLiving : Entity {
    public:
    float yaw = 0.0f, pitch = 0.0f;

    this(World world) @nogc @safe nothrow {
        super(world);
    }

    override Compound writeToNBT() const {
    	auto tag = super.writeToNBT();
    	tag["yaw"] = new Float(yaw);
    	tag["pitch"] = new Float(pitch);
    	return tag;
    }

    override void readFromNBT(Compound tag) {
    	super.readFromNBT(tag);

    	yaw = cast(Float)tag["yaw"];
    	pitch = cast(Float)tag["pitch"];
    }
}
