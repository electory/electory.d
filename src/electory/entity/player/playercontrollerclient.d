module electory.entity.player.playercontrollerclient;

import electory.entity.player.playercontroller;
import derelict.glfw3;
import electory.game.game;
import electory.client.tags;
import gfm.math;
import std.math;
import electory.block.block;
import electory.input.input;
import electory.utils.aabb;
import electory.utils.enums;
import electory.client.audio.audio;
import electory.resource.manager;

interface IPlayerControllerClient : IPlayerController {
    void processMouseEvent(MouseEvent event, ElectoryGame game);
}

class PlayerControllerClient : IPlayerControllerClient {
    EntityPlayer player;

    this(EntityPlayer player) {
        this.player = player;
    }

    override void update(ElectoryGame game) {
        float movementSpeed = player.isUnderwater ? 0.25f : (player.onGround ? 0.3f : 0.2f);

        if(auto tc = cast(IClient)game) /*if(!tc.guiHandler.blocksWorldInput) */{
            vec3f offset = vec3f(0.0f);

            mat4f rotationMatrix = mat4f.identity;

            rotationMatrix = rotationMatrix.rotateZ(-player.yaw);

            if(tc.getInputHandler().isKeyPressed(GLFW_KEY_W)) {
                offset.x += 1;
            }
            if(tc.getInputHandler().isKeyPressed(GLFW_KEY_S)) {
                offset.x -= 1;
            }
            if(tc.getInputHandler().isKeyPressed(GLFW_KEY_A)) {
                offset.y += 1;
            }
            if(tc.getInputHandler().isKeyPressed(GLFW_KEY_D)) {
                offset.y -= 1;
            }

            if(offset.squaredMagnitude != 0) offset = offset.normalized * movementSpeed; // Fix straferunning

            vec4f t = vec4f(offset.x, offset.y, offset.z, 1.0f);

            player.moveClipped((rotationMatrix * t).xyz);
        }
    }

    override vec3f getAcceleration(const ElectoryGame game) const {
        if(auto tc = cast(IClient)game) {
            return vec3f(0.0f, 0.0f,
                                    tc.getInputHandler().isKeyPressed(GLFW_KEY_SPACE) &&
                                        (player.onGround || player.isUnderwater) ? (player.isUnderwater ? 0.006f : 0.512f)
                                                                    : 0.0f);
        }
        assert(0); // never happens
    }

    override void processMouseEvent(MouseEvent event, ElectoryGame game) {
        player.yaw += event.dx * (PI / 180 * game.gameConfig.mouseSensitivity);
        player.pitch += event.dy * (PI / 180 * game.gameConfig.mouseSensitivity) * (game.gameConfig.invertMouseY ? 1 : -1);
        if(player.pitch < -PI / 2) {
            player.pitch = -PI / 2;
        } else if(player.pitch > PI / 2) {
            player.pitch = PI / 2;
        }

        if(event.button == GLFW_MOUSE_BUTTON_LEFT || event.button == GLFW_MOUSE_BUTTON_RIGHT) {
            if(event.action == GLFW_PRESS) {
                {
                    auto pos = player.pos;
                    pos += vec3f(0.0f, 0.0f, 1.5f);

                    ray3f ray = ray3f(vec3f(pos), vec3f(1.0f, 0.0f, 0.0f));

                    mat4f rotationMatrix = mat4f.identity;
                    rotationMatrix = mat4f.rotation(-player.yaw, vec3f(0f, 0f, 1f)) * mat4f.rotation(-player.pitch, vec3f(0f, 1f, 0f)) * rotationMatrix;

                    vec4f v4 = vec4f(ray.dir.x, ray.dir.y, ray.dir.z, 1.0f);
                    vec4f v4t = rotationMatrix * v4;
                    ray.dir = v4t.xyz;

                    int x1 = cast(int) floor(pos.x - 5);
                    int y1 = cast(int) floor(pos.y - 5);
                    int z1 = cast(int) floor(pos.z - 5);
                    int x2 = cast(int) ceil(pos.x + 5);
                    int y2 = cast(int) ceil(pos.y + 5);
                    int z2 = cast(int) ceil(pos.z + 5);

                    int tx = 0, ty = 0, tz = 0;

                    auto tres = AABBIntersectionResult!float(false, float.max);

                    for (int x = x1; x <= x2; x++) {
                        for (int y = y1; y <= y2; y++) {
                            for (int z = z1; z <= z2; z++) {
                                Block block = player.world.getBlockAt(x, y, z);
                                if (block !is null) {
                                    auto res = ray.intersectAABB(block.getAABB(player.world, x, y, z));
                                    if (res.hasHit && res.distance < tres.distance) {
                                        tres = res;
                                        tx = x;
                                        ty = y;
                                        tz = z;
                                    }
                                }
                            }
                        }
                    }

                    if (tres.hasHit && tres.distance <= 5.0f) {
                    	if(event.button == GLFW_MOUSE_BUTTON_LEFT) {
                            auto soundSource = new EntitySoundSource(player.world, ResourceLocation("base:audio/break.ogg"));
                            soundSource.setPosition(vec3f(tx + 0.5f, ty + 0.5f, tz + 0.5f), false);
                            player.world.addEntity(soundSource);
	                        player.world.setBlockAt(tx, ty, tz, null);
                    	} else if(player.selectedBlock !is null) {
                    		vec3i sidevec = tres.side.toVector!int;
                    		player.world.setBlockAt(tx + sidevec.x, ty + sidevec.y, tz + sidevec.z, player.selectedBlock);
                    	}
                    }
                }
            }
        }
    }
}