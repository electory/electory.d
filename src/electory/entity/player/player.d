module electory.entity.player.player;

import electory.entity.entity;
import electory.world.world;
import electory.client.audio.audio;
import electory.utils.aabb;
import electory.utils.enums;
import electory.block.block;
import electory.resource.manager;
import electory.entity.player.playercontroller;

import gfm.math.vector;
import gfm.math.matrix;
import gfm.math.shapes;
import derelict.glfw3;
import std.math;
import std.stdio;
import std.typecons;

class EntityPlayer : EntityLiving {
    public:

    Rebindable!(Block) selectedBlock = null;
    IPlayerController playerController;

    this(World world) @nogc @safe nothrow {
        super(world);
    }

    override void update() {
        super.update();

        if(playerController) playerController.update(world.tc);
    }

    override vec3f getAcceleration() const {
        return super.getAcceleration() + (playerController ? playerController.getAcceleration(world.tc) : vec3f(0));
    }
}