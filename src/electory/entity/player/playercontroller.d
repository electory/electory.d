module electory.entity.player.playercontroller;

import electory.game.game;
public import electory.entity.player.player;
import gfm.math;

interface IPlayerController {
    void update(ElectoryGame game);
    vec3f getAcceleration(const ElectoryGame game) const;
}