module electory.entity.registry;

import electory.resource.manager;
import electory.entity.entity;
import electory.world.world;

interface EntityInstantiationUnit {
	Entity instantiate(World world);
}

private class EntityInstantiationUnitImpl(T : Entity) : EntityInstantiationUnit {
	override Entity instantiate(World world) {
		return new T(world);
	}
}

class EntityRegistry {
	private:
	ResourceLocation[TypeInfo] entityToResourceMap;
	EntityInstantiationUnit[ResourceLocation] resourceToInstantiationUnitMap;

	public:
	this(ResourceManager resMan) {

	}

	ResourceLocation opIndex(TypeInfo entity) {
		return entityToResourceMap[entity];
	}

	EntityInstantiationUnit opIndex(ResourceLocation res) {
		return resourceToInstantiationUnitMap[res];
	}

	void register(T : Entity)(ResourceLocation resource) {
		entityToResourceMap[typeid(T)] = resource;
		resourceToInstantiationUnitMap[resource] = new EntityInstantiationUnitImpl!(T);
	}
}