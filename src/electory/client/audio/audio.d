module electory.client.audio.audio;

import derelict.openal.al;
import derelict.ogg.ogg;
import derelict.vorbis;
import gfm.math;
import std.stdio;
import std.string;
import std.file;
import std.system;
import std.variant;

import electory.entity.player.player;
import electory.entity.entity;
import electory.world.world;
import electory.resource.manager;

class AudioException : Exception {
    private static string openAlErrorToString(int err) {
        switch (err) {
            case AL_NO_ERROR: return "AL_NO_ERROR";
            case AL_INVALID_ENUM: return "AL_INVALID_ENUM";
            case AL_INVALID_VALUE: return "AL_INVALID_VALUE";
            case AL_OUT_OF_MEMORY: return "AL_OUT_OF_MEMORY";
            /* ... */
            default:
            return format!"Unknown error code %d"(err);
        }
    }

    this(ALenum errorcode, string file = __FILE__, size_t line = __LINE__) {
        super(openAlErrorToString(errorcode), file, line);
    }
}

struct OGGFile {
    OggVorbis_File vf;
    vorbis_info *vi;
    int bs = 0;

    this(ResourceManager resourceManager, ResourceLocation location) {
        string file = resourceManager.getResourcePath(location);
        int fopenerr = ov_fopen(file.toStringz(), &vf);
        if(fopenerr) {
            writefln!"error %d\n"(fopenerr);
        }

        vi = ov_info(&vf, -1);
        assert(vi);
    }

    int channels() {
        return vi.channels;
    }

    int sampleRate() {
        return vi.rate;
    }

    long readSamples(short[] targetBuffer) {
        return ov_read(&vf, cast(byte*)targetBuffer.ptr, cast(int)(targetBuffer.length * targetBuffer[0].sizeof), endian == Endian.bigEndian ? 1 : 0, 2, 1, &bs);
    }

    short[] readAllSamples() {
        short[] buf = new short[](0);
        short[] tempBuf = new short[](4096);

        long count;

        while((count = readSamples(tempBuf)) != 0) {
            long scount = count / short.sizeof;
            buf ~= tempBuf[0..scount];
        }
        
        return buf;
    }

    ~this() {
        ov_clear(&vf);
    }
}

class EntitySoundSource : Entity {
    private:
    ResourceLocation soundPath;
    bool initialized = false;
    bool gonnaDie = false;
    Variant soundState;

    public:
    this(World world, ResourceLocation soundPath) @nogc @safe nothrow {
        super(world);
        this.soundPath = soundPath;
    }

    override void update() {
        //initialized = true;
        
    }

    override void scheduleRemove() {
        gonnaDie = true;
    }

    void scheduleRealRemove() {
        super.scheduleRemove();
    }


    override bool isTransient() {
        return true;
    }
}

class AudioSystem {
    private:
    ALCdevice* device;
    ALCcontext* context;

    public:
    this() {

    }

    void init() {
        DerelictAL.load();
        DerelictOgg.load();
        DerelictVorbis.load();
        DerelictVorbisEnc.load();
        DerelictVorbisFile.load();

        device = alcOpenDevice(null);
        if(device is null) {
            return; // Cannot init audio
        }
        context = alcCreateContext(device, null);
        if (!alcMakeContextCurrent(context)) {
            return; // Failed context
        }
    }

    void checkError() {
        ALCenum error = alGetError();
        if(error != AL_NO_ERROR) {
            throw new AudioException(error);
        }
    }

    void updateListener(EntityPlayer player) {
        alListener3f(AL_POSITION, player.pos.x, player.pos.y, player.pos.z);
        checkError();
        alListener3f(AL_VELOCITY, player.velocity.x, player.velocity.y, player.velocity.z);
        checkError();
        vec4f v1 = vec4f(1.0f, 0.0f, 0.0f, 1.0f);
        mat4f rotationMatrix = mat4f.identity;
        rotationMatrix = mat4f.rotation(-player.yaw, vec3f(0f, 0f, 1f)) * mat4f.rotation(-player.pitch, vec3f(0f, 1f, 0f)) * rotationMatrix;
        vec3f fv = (rotationMatrix * v1).xyz;
        float[] ori = [fv.x, fv.y, fv.z, 0f, 0f, 1f];
        alListenerfv(AL_ORIENTATION, ori.ptr);
        checkError();
    }

    private ALuint[ResourceLocation] loadedSounds;

    private ALuint loadSound0(ResourceManager resMan, ResourceLocation resLoc) {
        OGGFile oggFile = OGGFile(resMan, resLoc);

        auto allSamples = oggFile.readAllSamples();

        ALuint buf;
        alGenBuffers(1, &buf);
        checkError();

        alBufferData(buf, oggFile.channels == 2 ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16, cast(const ALvoid*) allSamples,
                     cast(int)(allSamples.length * allSamples[0].sizeof), oggFile.sampleRate);
        checkError();
        
        return buf;
    }

    ALuint getSound(ResourceManager resMan, ResourceLocation resLoc) {
        if(resLoc in loadedSounds) {
            return loadedSounds[resLoc];
        }
        return loadedSounds[resLoc] = loadSound0(resMan, resLoc);
    }

    void updateSoundEntities(World world) {
        foreach(entity; world.getEntities) {
            if(auto ss = cast(EntitySoundSource)entity) {
                if(ss.removeScheduled) continue;
                if(!ss.initialized) {
                    ALuint buffer = getSound(world.tc.resourceManager, ss.soundPath);
                    ALuint source;
                    alGenSources(1, &source);
                    checkError();
                    ss.soundState = source;
                    alSourcef(source, AL_PITCH, 1);
                    alSourcef(source, AL_GAIN, 1);
                    alSource3f(source, AL_POSITION, ss.pos.x, ss.pos.y, ss.pos.z);
                    alSource3f(source, AL_VELOCITY, ss.velocity.x, ss.velocity.y, ss.velocity.z);
                    alSourcei(source, AL_SOURCE_RELATIVE, 0);
                    alSourcei(source, AL_LOOPING, AL_FALSE);
                    alSourcei(source, AL_BUFFER, buffer);
                    checkError();
                    alSourcePlay(source);
                    checkError();
                    ss.initialized = true;
                }
                ALuint source = ss.soundState.get!ALuint;

                ALenum source_state = AL_PLAYING;
                alGetSourcei(source, AL_SOURCE_STATE, &source_state);
                checkError();
                if(source_state != AL_PLAYING || ss.gonnaDie) {
                    entity.scheduleRemove();
                    alDeleteSources(1, &source);
                    checkError();
                    ss.scheduleRealRemove();
                }
            }
        }
    }
}
