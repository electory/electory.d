module electory.client.timer;

import std.math;
import core.time;

struct TickTimer {
    private:
        long lastTime = 0;
        double sysTicksPerTick;
    public:
        int elapsedTicks = 0;

        float renderPartialTicks = 0;

        long totalTicks = 0;

        this(float tps) {
            sysTicksPerTick = MonoTime.ticksPerSecond / tps;
        }

        void updateTimer() {
            if (lastTime == 0) {
                lastTime = MonoTime.currTime.ticks;
            }

            elapsedTicks = 0;

            long now = MonoTime.currTime.ticks;
            renderPartialTicks += (now - lastTime) / sysTicksPerTick;

            lastTime = now;


            elapsedTicks = cast(int)floor(renderPartialTicks);
            renderPartialTicks -= elapsedTicks;

            if (elapsedTicks > 10) {
                elapsedTicks = 10;
            }

            totalTicks += elapsedTicks;

        }
}