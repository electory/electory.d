module electory.client.event.atlas;

import electory.utils.signals;
import electory.client.render.atlas;

/**
 * This signal is emitted when an atlas is preparing the sprites
 */
class AtlasStitchSignal {
    mixin ESignal!(AtlasSpriteManager);
}

/// Signal that is emitted when a GUI atlas is being prepared
AtlasStitchSignal guiAtlasStitchSignal;

static this() {
    guiAtlasStitchSignal = new AtlasStitchSignal;
}