module electory.client.gui.canvas;

import gfm.opengl;
import gfm.math;
import derelict.opengl;
import std.array;
import std.stdio;
import std.traits;
import std.typecons;
import std.algorithm.iteration;
import std.container.slist;
import electory.client.render.atlas;

struct VectorWithUV(T, int NT, U, int NU) {
    Vector!(T, NT) position;
    Vector!(U, NU) texCoord;

    /*alias x = position.x;
    static if(NT >= 2) alias y = position.y;
    static if(NT >= 3) alias z = position.z;
    static if(NT >= 4) alias w = position.w;

    alias u = texCoord.x;
    static if(NU >= 2) alias v = texCoord.y;
    static if(NU >= 3) alias p = texCoord.z;
    static if(NU >= 4) alias q = texCoord.w;*/

    this(typeof(position) position, typeof(texCoord) texCoord) {
        this.position = position;
        this.texCoord = texCoord;
    }

    this(A...)(A args) {
        position = typeof(position)(args[0..NT]);
        texCoord = typeof(texCoord)(args[NT..$]);
    }
}

alias vec2fuv2f = VectorWithUV!(float, 2, float, 2);

class CanvasList(T) { // T = vertex type
    @disable this();

    OpenGL gfmgl;
    Unique!GLBuffer vbo;
    Unique!GLVAO vao;
    T[] vertexes;
    private SList!(IVertexTransform!T) transforms;
    private T protoVertex;

    alias VertexType = T;

    this(OpenGL gfmgl, VertexSpecification!T vspec) {
        this.gfmgl = gfmgl;
        this.vao = new GLVAO(gfmgl);
        this.vbo = new GLBuffer(gfmgl, GL_ARRAY_BUFFER, GL_STREAM_DRAW);

        this.vao.bind();
        this.vbo.bind();
        vspec.use();
        this.vao.unbind();
    }

    void pushVertexTransform(IVertexTransform!T transform) {
        transforms.insert(transform);
    }

    void popVertexTransform(int count = 1) {
        assert(count >= 1, "popping <1? wtf");
        transforms.removeFront(count);
    }

    void opOpAssign(string op)(typeof(T.position) pos) if(op == "~") {
        T vertex = protoVertex;
        vertex.position = pos;
        foreach(transform; transforms) {
            vertex = transform.transform(vertex);
        }
        vertexes ~= vertex;
    }

    void opOpAssign(string op, Q)(Q vd) if(op == "~" && is(Q == VectorWithUV!(A, B, C, D), A, int B, C, int D)) {
        T vertex = protoVertex;
        vertex.position = vd.position;
        vertex.texCoord = vd.texCoord;
        foreach(transform; transforms) {
            vertex = transform.transform(vertex);
        }
        vertexes ~= vertex;
    }

    auto @property opDispatch(string name, U)(U val) {
        return mixin("protoVertex." ~ name) = val;
    }

    auto @property opDispatch(string name)() {
        return mixin("protoVertex." ~ name);
    }

    void flush(GLProgram shader) {
        shader.use();
        this.vao.bind();
        vbo.setData(vertexes);
        glDrawArrays(GL_TRIANGLES, 0, cast(int)(vbo.size / T.sizeof));
        this.vao.unbind();
        vertexes = [];
    }

    /*~this() {
        vao.destroy;
        vbo.destroy;
    }*/
}

interface IVertexTransform(T) {
    T transform(T input);
}

class AtlasSpriteVertexTransform(T) : IVertexTransform!T {
    AtlasSprite sprite;

    this(AtlasSprite sprite) {
        this.sprite = sprite;
    }

    override T transform(T input) {
        input.texCoord = lerp(vec2f(sprite.getMinU, sprite.getMinV), vec2f(sprite.getMaxU, sprite.getMaxV), input.texCoord);
        return input;
    }
}

class Canvas(T) {
    @disable this();

    OpenGL gfmgl;
    GLProgram shader;
    VertexSpecification!T vspec;

    alias VertexType = T;

    this(OpenGL gfmgl, GLProgram shader) {
        this.gfmgl = gfmgl;
        this.shader = shader;
        this.vspec = new VertexSpecification!T(shader);

        foreach(i; 0..32) {
            this.listPool.insert(new CanvasList!T(gfmgl, vspec));
        }
    }

    ~this() {
        assert(lists.empty, "destroying canvas with some lists");
        foreach(list; listPool) {
            list.destroy!false;
        }
    }

    void bindTexture(GLTexture texture) {
        if(!lists.empty) {
            topList.flush(shader);
        }
        texture.use(0);
        shader.uniform("texture").set!int(0);
    }
    
    void begin() {
        if(!lists.empty) {
            topList.flush(shader);
        }
        lists.insert(this.listPool.front); // acquire
        this.listPool.removeFront();       //
    }

    void opOpAssign(string op, T)(T val) if(op == "~") {
        topList ~= val;
    }

    auto @property opDispatch(string name, U)(U val) {
        return mixin("topList." ~ name ~ "=val");
    }

    auto @property opDispatch(string name)() {
        return mixin("topList." ~ name);
    }

    void flush() {
        topList.flush(shader);
    }

    void end() {
        topList.flush(shader);
        // topList.destroy!false;
        listPool.insert(lists.front); // release
        lists.removeFront();          //
    }

    private:
    ref CanvasList!T topList() {
        assert(!lists.empty, "no canvas list open");
        return lists.front;
    }

    SList!(CanvasList!T) lists;
    SList!(CanvasList!T) listPool;
}

void addRect(T, U1, U2)(T canvas, VectorWithUV!(U1, 2, U2, 2) pos1, VectorWithUV!(U1, 2, U2, 2) pos2) {
    canvas ~= VectorWithUV!(U1, 2, U2, 2)(pos2.position.x, pos2.position.y, pos2.texCoord.x, pos2.texCoord.y);
    canvas ~= VectorWithUV!(U1, 2, U2, 2)(pos2.position.x, pos1.position.y, pos2.texCoord.x, pos1.texCoord.y);
    canvas ~= VectorWithUV!(U1, 2, U2, 2)(pos1.position.x, pos1.position.y, pos1.texCoord.x, pos1.texCoord.y);

    canvas ~= VectorWithUV!(U1, 2, U2, 2)(pos1.position.x, pos1.position.y, pos1.texCoord.x, pos1.texCoord.y);
    canvas ~= VectorWithUV!(U1, 2, U2, 2)(pos1.position.x, pos2.position.y, pos1.texCoord.x, pos2.texCoord.y);
    canvas ~= VectorWithUV!(U1, 2, U2, 2)(pos2.position.x, pos2.position.y, pos2.texCoord.x, pos2.texCoord.y);
}