module electory.client.gui.font;

import electory.client.event.atlas;
import electory.client.render.atlas;
import electory.client.render.data;
import electory.client.gui.canvas;
import electory.resource.manager;

class FontRenderer {
    AtlasSpriteId fontMap;
    AtlasSpriteManager spriteManager;

    void initFontMap(AtlasSpriteManager manager) {
        this.fontMap = manager.registerSprite(ResourceLocation("base:textures/font/u00.png"));
        this.spriteManager = manager;
    }

    private void drawChar(Canvas!UIVertex canvas, float x, float y, dchar c) {
        immutable float minU = spriteManager.getSpriteById(fontMap).getMinU();
        immutable float minV = spriteManager.getSpriteById(fontMap).getMinV();
        immutable float maxU = spriteManager.getSpriteById(fontMap).getMaxU();
        immutable float maxV = spriteManager.getSpriteById(fontMap).getMaxV();

        immutable float sizU = maxU - minU;
        immutable float sizV = maxV - minV;

        immutable float cszU = sizU / 16;
        immutable float cszV = sizV / 16;

        immutable float mncU = minU + cszU * (c % 16);
        immutable float mncV = minV + cszV * (c / 16);
        immutable float mxcU = mncU + cszU;
        immutable float mxcV = mncV + cszV;

        canvas.addRect(vec2fuv2f(x, y, mncU, mncV), vec2fuv2f(x + 8, y + 8, mxcU, mxcV));
    }

    int getTextWidth(string str) {
        int len = 0;
        foreach(dchar c; str) len++; // dont think i'm a moron. it's to take unicode into account
        return len * 8;
    }

    int getCharHeight() {
        return 8;
    }

    void drawText(Canvas!UIVertex canvas, float x, float y, string str) {
        foreach(dchar c; str) {
            drawChar(canvas, x, y, c);
            x += 8;
        }
    }
}

FontRenderer fontRenderer;

void initFontRenderer() {
    fontRenderer = new FontRenderer;
    guiAtlasStitchSignal.connect(&fontRenderer.initFontMap);
}