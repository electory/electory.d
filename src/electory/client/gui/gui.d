module electory.client.gui.gui;

import electory.client.game;
import electory.client.gui.canvas;
import electory.client.gui.font;
import electory.client.render.atlas;
import electory.client.render.data;
import electory.client.render.render;
import electory.client.render.shader;
import electory.resource.manager;
import electory.input.input;
import electory.client.event.atlas;

import gfm.math;
import std.stdio;
import std.format;
import std.functional;
import derelict.glfw3;
import derelict.opengl;

struct GuiHandler {
    private GuiScreen currentGui;
    GuiScreen hud;

    private Canvas!UIVertex canvas;

    AtlasSpriteManagerImplStitch spriteManager;

    AtlasSpriteId whiteAtlasSprite;

    /// Initializes gui sprite atlas
    void initAtlas() {
        spriteManager = new AtlasSpriteManagerImplStitch(
            Electory.instance.resourceManager,
            Electory.instance.renderer.textureManager,
            ResourceLocation("base:textures/atlas/gui.png")
        );
        guiAtlasStitchSignal.emit(spriteManager);
        whiteAtlasSprite = spriteManager.registerSprite(ResourceLocation("base:textures/gui/white.png"));
        spriteManager.buildAtlas();
    }

    void setHUD(GuiScreen hud) {
        this.hud = hud;
        int w, h;
        glfwGetFramebufferSize(Electory.instance.renderer.window, &w, &h);
        hud.updateScreenDims(w, h);
    }

    /// Ticks all guis
    void tick() {
        if(hud) hud.tick();
        if(currentGui) currentGui.tick();
    }

    /// Opens a GUI
    void openGui(GuiScreen gui) {
        if(gui is null) {
            if(currentGui) {
                currentGui = currentGui.closeGui();
            }
        } else {
            if(currentGui) currentGui.closeGui();
            int w, h;
            glfwGetFramebufferSize(Electory.instance.renderer.window, &w, &h);
            gui.updateScreenDims(w, h);
            currentGui = gui;
        }
    }

    /// Closes the current GUI
    void closeGui() {
        openGui(null);
    }

    /// Processes mouse event
    void processMouseEvent(MouseEvent event) {
        if(currentGui) currentGui.processMouseEvent(event);
    }

    /// Processes keyboard event
    void processKeyboardEvent(KeyboardEvent event) {
        if(currentGui) currentGui.processKeyboardEvent(event);
    }

    /// Renders all GUIs
    void render(Renderer renderer) {
        if(currentGui || Electory.instance.world) {
            if(!canvas) canvas = new Canvas!UIVertex(
                                                renderer.gfmgl,
                                                renderer.shaderManager.getProgram(
                                                    ResourceLocation("base:shaders/gui.toml")
                                                )
                                              );

            int w, h;
            glfwGetFramebufferSize(renderer.window, &w, &h);

            renderer.projectionMatrixStack.push();
            renderer.projectionMatrixStack.loadIdentity();
            renderer.projectionMatrixStack.ortho(0, w, h, 0, -1.0f, 1.0f);

            updateMatrices();

            canvas.bindTexture(renderer.textureManager.getTexture2D(ResourceLocation("base:textures/atlas/gui.png")));

            glEnable(GL_BLEND);

            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            canvas.begin();
            if(Electory.instance.world && hud) {
                hud.render(canvas);
            }
            if(currentGui) {
                currentGui.render(canvas);
            }
            canvas.end();

            glDisable(GL_BLEND);

            renderer.projectionMatrixStack.pop();
        }
    }

    /// Updates matrices from the stack
    void updateMatrices() {
        auto program = Electory.instance.renderer.shaderManager.getProgram(ResourceLocation("base:shaders/gui.toml"));

        program.uniform("projectionMatrix").set(Electory.instance.renderer.projectionMatrixStack.top);
        program.uniform("viewMatrix").set(Electory.instance.renderer.viewMatrixStack.top);
        program.uniform("modelMatrix").set(Electory.instance.renderer.modelMatrixStack.top);
    }

    ~this() {
        canvas.destroy!false;
    }

    @disable this(ref return scope const GuiHandler);
}

GuiHandler guiHandler;

class Gui {
    this() {
    }

    /// Renders this GUI
    abstract void render(Canvas!UIVertex canvas);
    /// Called when GUI ticks
    void tick() {}
    /// Called when mouse event is fired for this GUI
    void processMouseEvent(MouseEvent) {}
    /// Called when keyboard event is fired for this GUI
    void processKeyboardEvent(KeyboardEvent) {}
}

class GuiScreen : Gui {
    this() {
        super();
    }

    /// Called on screen dimension change
    abstract void updateScreenDims(int width, int height);

    /**
     * Returns: next GUI (useful for buttons like <back>)
     */
    GuiScreen closeGui() {
        return null;
    }
}

class GuiWidget : Gui {
    /// Constructs
    this() {
        super();
    }

    protected vec2f position = void;
    protected vec2f size = void;

    private GuiScreenWidgets parentScreen_;
    /// Parent widget of this widget
    GuiWidget parent;

    /// Sets the parent screen of widget
    @property GuiScreenWidgets parentScreen(GuiScreenWidgets screen) {
        parentScreen_ = screen;
        return screen;
    }

    /// Gets the parent screen of widget
    @property GuiScreenWidgets parentScreen() {
        return parentScreen_;
    }

    /// Measures the widget
    abstract vec2f initialMeasure();

    /// Does the final layout pass
    void finalLayout(vec2f position, vec2f measurement, vec2f parentDims, vec2f layoutMeasurement) {
        this.position = position;
        this.size = layoutMeasurement;
    }

    /// Traverses down for the final layout pass
    void traverseFinalLayout(vec2f position, vec2f measurement, vec2f parentDims, vec2f layoutMeasurement) {
        finalLayout(position, measurement, parentDims, layoutMeasurement);
    }
}

enum GUI_WIDGET_EXPAND = -1.0f;
enum GUI_WIDGET_SHRINK = -2.0f;

class GuiScreenWidgets : GuiScreen {
    protected GuiWidget rootWidget;
    vec2i screenDims = vec2i(0, 0);

    this(GuiWidget rootWidget) {
        super();
        rootWidget.parentScreen = this;
        this.rootWidget = rootWidget;
    }

    override void updateScreenDims(int width, int height) {
        screenDims = vec2i(width, height);
        relayout();
    }

    override void tick() {
        rootWidget.tick();
    }

    void relayout() {
        vec2f initialMeasurement = rootWidget.initialMeasure();
        vec2f layoutMeasurement = initialMeasurement;
        if(layoutMeasurement.x == GUI_WIDGET_EXPAND) layoutMeasurement.x = screenDims.x;
        if(layoutMeasurement.y == GUI_WIDGET_EXPAND) layoutMeasurement.y = screenDims.y;

        rootWidget.traverseFinalLayout(vec2f(0, 0), initialMeasurement, vec2f(screenDims.x, screenDims.y), layoutMeasurement);
    }

    override void render(Canvas!UIVertex canvas) {
        rootWidget.render(canvas);
    }
}

enum FlowDir {
    HORIZONTAL = 0, VERTICAL = 1
}

class GuiWidgetLinearLayout(FlowDir dir) : GuiWidget {
    private GuiWidget[] children;

    private float gap;

    this(float gap = 0) {
        super();
        this.gap = gap;
    }

    override vec2f initialMeasure() {
        vec2f ret = vec2f(0, 0);
        int first = true;
        foreach(child; children) {
            immutable childMeasure = child.initialMeasure();
            static if(dir == FlowDir.HORIZONTAL) {
                ret.x += childMeasure.x; // we're not using EXPAND because this layout is fixed-size
                if(!first) ret.x += gap;
                if(childMeasure.y > ret.y) ret.y = childMeasure.y;
            } else static if(dir == FlowDir.VERTICAL) {
                ret.y += childMeasure.y; // we're not using EXPAND because this layout is fixed-size
                if(!first) ret.y += gap;
                if(childMeasure.x > ret.x) ret.x = childMeasure.x;
            }
            first = false;
        }
        return ret;
    }

    override void tick() {
        foreach(child; children) child.tick();
    }

    // TODO: fix O(n^2) measurement

    override void traverseFinalLayout(vec2f position, vec2f measurement, vec2f parentDims, vec2f layoutMeasurement) {
        vec2f childPos = position;
        foreach(child; children) {
            vec2f initialMeasurementValue = child.initialMeasure();
            vec2f finalMeasurementValue = void;
            
            static if(dir == FlowDir.HORIZONTAL) {
                finalMeasurementValue = vec2f(initialMeasurementValue.x, layoutMeasurement.y);
            } else static if(dir == FlowDir.VERTICAL) {
                finalMeasurementValue = vec2f(layoutMeasurement.x, initialMeasurementValue.y);
            }

            child.traverseFinalLayout(
                                      childPos, initialMeasurementValue,
                                      vec2f(GUI_WIDGET_SHRINK, GUI_WIDGET_SHRINK),
                                      finalMeasurementValue
                                     );
            static if(dir == FlowDir.HORIZONTAL) {
                childPos.x += initialMeasurementValue.x + gap;
            } else static if(dir == FlowDir.VERTICAL) {
                childPos.y += initialMeasurementValue.y + gap;
            }
        }
        super.traverseFinalLayout(position, measurement, parentDims, layoutMeasurement);
    }

    void opOpAssign(string op)(GuiWidget widget) if(op == "~") {
        children ~= widget;
        widget.parent = this;
        widget.parentScreen = super.parentScreen;
        if(super.parentScreen) super.parentScreen.relayout();
    }

    override @property GuiScreenWidgets parentScreen(GuiScreenWidgets screen) {
        foreach(child; children) child.parentScreen = screen;

        return super.parentScreen = screen;
    }

    override void render(Canvas!UIVertex canvas) {
        foreach(child; children) {
            child.render(canvas);
        }
    }
}

struct LayoutEntry(T) {
    GuiWidget widget;
    T layoutData;
}

enum SidedLayoutSide {
    TOP = 1,
    BOTTOM = 2,
    CENTER_VERTICAL = TOP | BOTTOM,
    LEFT = 4,
    RIGHT = 8,
    CENTER_HORIZONTAL = LEFT | RIGHT,
    TOP_LEFT = TOP | LEFT,
    CENTER_LEFT = CENTER_VERTICAL | LEFT,
    BOTTOM_LEFT = BOTTOM | LEFT,
    TOP_CENTER = TOP | CENTER_HORIZONTAL,
    CENTER = CENTER_VERTICAL | CENTER_HORIZONTAL,
    BOTTOM_CENTER = BOTTOM | CENTER_HORIZONTAL,
    TOP_RIGHT = TOP | RIGHT,
    CENTER_RIGHT = CENTER_VERTICAL | RIGHT,
    BOTTOM_RIGHT = BOTTOM | RIGHT,
}

struct SidedLayoutData {
    int x;
    int y;
    SidedLayoutSide side;
}

class GuiWidgetSidedLayout : GuiWidget {
    private LayoutEntry!SidedLayoutData[] children;

    this() {
        super();
    }

    override vec2f initialMeasure() {
        return vec2f(GUI_WIDGET_EXPAND, GUI_WIDGET_EXPAND);
    }

    void opOpAssign(string op)(LayoutEntry!SidedLayoutData entry) if(op == "~") {
        children ~= entry;
        entry.widget.parent = this;
        entry.widget.parentScreen = super.parentScreen;
        if(super.parentScreen) super.parentScreen.relayout();
    }

    override void render(Canvas!UIVertex canvas) {
        foreach(entry; children) {
            entry.widget.render(canvas);
        }
    }

    override @property GuiScreenWidgets parentScreen(GuiScreenWidgets screen) {
        foreach(entry; children) entry.widget.parentScreen = screen;

        return super.parentScreen = screen;
    }

    override void traverseFinalLayout(vec2f position, vec2f measurement, vec2f parentDims, vec2f layoutMeasurement) {
        foreach(entry; children) {
            vec2f initialMeasurementValue = entry.widget.initialMeasure();

            vec2f pos = void;
            vec2f finalMeasurementValue = void;

            if(initialMeasurementValue.x == GUI_WIDGET_EXPAND) {
                finalMeasurementValue.x = layoutMeasurement.x - entry.layoutData.x * 2;
                pos.x = entry.layoutData.x;
            } else {
                finalMeasurementValue.x = initialMeasurementValue.x;
                if((entry.layoutData.side & SidedLayoutSide.CENTER_HORIZONTAL) == SidedLayoutSide.CENTER_HORIZONTAL) {
                    pos.x = (layoutMeasurement.x - initialMeasurementValue.x) / 2;
                } else if((entry.layoutData.side & SidedLayoutSide.LEFT) == SidedLayoutSide.LEFT) {
                    pos.x = entry.layoutData.x;
                } else if((entry.layoutData.side & SidedLayoutSide.RIGHT) == SidedLayoutSide.RIGHT) {
                    pos.x = layoutMeasurement.x - initialMeasurementValue.x - entry.layoutData.x;
                }
            }

            if(initialMeasurementValue.y == GUI_WIDGET_EXPAND) {
                finalMeasurementValue.y = layoutMeasurement.y - entry.layoutData.y * 2;
                pos.y = entry.layoutData.y;
            } else {
                finalMeasurementValue.y = initialMeasurementValue.y;
                if((entry.layoutData.side & SidedLayoutSide.CENTER_VERTICAL) == SidedLayoutSide.CENTER_VERTICAL) {
                    pos.y = (layoutMeasurement.y - initialMeasurementValue.y) / 2;
                } else if((entry.layoutData.side & SidedLayoutSide.TOP) == SidedLayoutSide.TOP) {
                    pos.y = entry.layoutData.y;
                } else if((entry.layoutData.side & SidedLayoutSide.BOTTOM) == SidedLayoutSide.BOTTOM) {
                    pos.y = layoutMeasurement.y - initialMeasurementValue.y - entry.layoutData.y;
                }
            }

            pos += position;

            entry.widget.traverseFinalLayout(pos, initialMeasurementValue, layoutMeasurement, finalMeasurementValue);
        }
        super.traverseFinalLayout(position, measurement, parentDims, layoutMeasurement);
    }
}

class GuiWidgetLabel : GuiWidget {
    private string text_;

    this(string text_) {
        super();
        this.text_ = text_;
    }

    override vec2f initialMeasure() {
        return vec2f(fontRenderer.getTextWidth(text_), fontRenderer.getCharHeight());
    }

    override void render(Canvas!UIVertex canvas) {
        fontRenderer.drawText(canvas, position.x, position.y, text_);
    }

    @property string text(string val) {
        bool changed = text_ != val;
        text_ = val;
        if(changed && parentScreen) {
            parentScreen.relayout();
        }
        return text_;
    }

    @property string text() {
        return text_;
    }
}

class GuiWidgetHotbarSlot : GuiWidget {
    this() {

    }

    override vec2f initialMeasure() {
        return vec2f(32, 32);
    }

    override void render(Canvas!UIVertex canvas) {
        canvas.color = vec4f(0, 0, 0, 0.5);

        canvas.pushVertexTransform(new AtlasSpriteVertexTransform!UIVertex(guiHandler.spriteManager.getSpriteById(guiHandler.whiteAtlasSprite)));
        canvas.addRect(vec2fuv2f(position, vec2f(0, 0)), vec2fuv2f(position + size, vec2f(0, 0)));
        canvas.popVertexTransform();

        canvas.color = vec4f(1, 1, 1, 1);
    }
}

class GuiWidgetCrosshair : GuiWidget {
    private AtlasSpriteId sprite;
    private int width;
    private int height;

    this(AtlasSpriteId sprite, int width, int height) {
        this.sprite = sprite;
        this.width = width;
        this.height = height;
    }

    override vec2f initialMeasure() {
        return vec2f(width, height);
    }

    override void render(Canvas!UIVertex canvas) {
        canvas.flush();
        glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO);
        canvas.pushVertexTransform(new AtlasSpriteVertexTransform!UIVertex(guiHandler.spriteManager.getSpriteById(sprite)));
        canvas.addRect(vec2fuv2f(position, vec2f(0, 0)), vec2fuv2f(position + size, vec2f(1, 1)));
        canvas.popVertexTransform();
        canvas.flush();
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
}

class GuiInGame : GuiScreenWidgets {
    GuiWidgetLabel debugInfoLabel;
    GuiWidgetHotbarSlot[] hotbarSlots;
    static AtlasSpriteId crosshairSprite;

    static this() {
        guiAtlasStitchSignal.connect(&initAtlas);
    }

    static void initAtlas(AtlasSpriteManager manager) {
        crosshairSprite = manager.registerSprite(ResourceLocation("base:textures/gui/crosshair.png"));
    }

    this() {
        auto sidedLayout = new GuiWidgetSidedLayout();
        {
            auto layout = new GuiWidgetLinearLayout!(FlowDir.VERTICAL)();
            layout ~= new GuiWidgetLabel("Electory v0.0.12");
            layout ~= debugInfoLabel = new GuiWidgetLabel("");
            sidedLayout ~= LayoutEntry!SidedLayoutData(layout, SidedLayoutData(8, 8, SidedLayoutSide.TOP_LEFT));
        }
        {
            auto layout = new GuiWidgetLinearLayout!(FlowDir.HORIZONTAL)(8);
            foreach(i; 0..9) {
                auto slot = new GuiWidgetHotbarSlot();
                hotbarSlots ~= slot;
                layout ~= slot;
            }
            sidedLayout ~= LayoutEntry!SidedLayoutData(layout, SidedLayoutData(0, 8, SidedLayoutSide.BOTTOM_CENTER));
        }
        {
            sidedLayout ~= LayoutEntry!SidedLayoutData(new GuiWidgetCrosshair(crosshairSprite, 8, 8), SidedLayoutData(0, 0, SidedLayoutSide.CENTER));
        }
        super(sidedLayout);
    }

    override void tick() {
        debugInfoLabel.text = format!"%d FPS, %d CU"(Electory.instance.renderer.fps, Electory.instance.renderer.worldRenderer.chunkUpdatesCounter);
    }
}
