module electory.client.game;

import derelict.glfw3;
import gfm.opengl;
import standardpaths;
import std.experimental.logger;
import std.stdio;
import std.string;
import std.file;
import std.path;
import std.array;
import core.runtime;
import core.sys.windows.windows;
import core.stdc.stdio;

import electory.world.world;
import electory.world.chunk;
import electory.resource.manager;
import electory.client.render.shader;
import electory.client.render.render;
import electory.client.timer;
import electory.entity.player.playercontrollerclient;
import electory.game.game;
import electory.input.input;
import electory.client.gui.gui;
import electory.client.gui.font;
import electory.client.audio.audio;
import electory.client.tags;

class Electory : ElectoryGame, IClient {
    public:
    Logger logger;
    World world;
    Renderer renderer;
    AudioSystem audioSystem;
    TickTimer timer;
    InputHandler inputHandler;

    this() {

    }

    override Renderer getRenderer() {
        return renderer;
    }

    override InputHandler getInputHandler() {
        return inputHandler;
    }

    override void preInit() {
        super.preInit();
        inputHandler = new InputHandler(this);
        timer = TickTimer(20.0f);
    }

    override void init() {
        super.init();
    }

    void initRenderer() {
        renderer = new Renderer(this);
        renderer.init();

        audioSystem = new AudioSystem();
        audioSystem.init();

        initFontRenderer();
        guiHandler.initAtlas();
    }

    void initGame() {
        guiHandler.setHUD(new GuiInGame());
        world = new World(this);
        world.init;
        world.load;
    }

    void update() {
        timer.updateTimer();
        if(!world.currentPlayer.playerController) world.currentPlayer.playerController = new PlayerControllerClient(world.currentPlayer);
        foreach(tickNumber; 0..timer.elapsedTicks) {
            if(world) world.update();
            guiHandler.tick();
        }
        audioSystem.updateSoundEntities(world);
        audioSystem.updateListener(world.currentPlayer);
    }

    void start() {
        preInit();

        init();

        initRenderer();
        initGame();

        while(renderer.isInLoop()) {
            glfwPollEvents();
            update();
            renderer.render(timer.renderPartialTicks);
        }

        world.unloadSafe();
        destroy(world);
        destroy(renderer);
    }

    override string getUserDataFolder() {
    	string rp;
    	version(Windows) {
	    	rp = writablePath(StandardPath.roaming);
    	} else {
    		rp = writablePath(StandardPath.data);
    	}
    	return chainPath(rp, "electory").array;
    }

    static auto instance() {
        static Electory instance;
        if(!instance) {
            instance = new Electory;
        }
        return instance;
    }
}


version(Windows) {
    extern(Windows) int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
        int result = 0;

        try {
            Runtime.initialize();
            if(!AttachConsole(ATTACH_PARENT_PROCESS)) {
                freopen("NUL", "w", core.stdc.stdio.stdout);
                freopen("NUL", "w", core.stdc.stdio.stderr);
            }
            Electory.instance.start();
            Runtime.terminate();
        } catch (Throwable e) {
            MessageBoxA(null, e.toString().toStringz(), null,
                        MB_ICONEXCLAMATION);
            result = 1;     // failed
        }

        return result;
    }
} else {
    void main() {
        Electory.instance.start();
    }
}