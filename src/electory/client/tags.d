module electory.client.tags;

import electory.client.render.render;
import electory.input.input;

interface IClient {
    Renderer getRenderer();
    InputHandler getInputHandler();
}