module electory.client.render.render;

import derelict.glfw3;
import gfm.opengl;
import gfm.math;
import std.experimental.logger;
import std.stdio;
import std.conv;
import core.time;

import electory.client.game;
import electory.client.render.shader;
import electory.client.render.texture;
import electory.client.render.world;
import electory.client.gui.gui;
// import electory.client.render.gui;
import electory.ver;
import electory.input.input;

class Renderer {
    public:
    GLFWwindow *window;
    OpenGL gfmgl;
    ShaderManager shaderManager;
    TextureManager textureManager;
    Logger logger;
    Electory tc;
    MatrixStack!(4,float) projectionMatrixStack;
    MatrixStack!(4,float) viewMatrixStack;
    MatrixStack!(4,float) modelMatrixStack;
    WorldRenderer worldRenderer;
    int fps;
    int frameCounter;
    MonoTime fpsTimer;
    // GuiRenderer guiRenderer;

    this(Electory tc) nothrow @safe @nogc {
        this.tc = tc;
        this.fpsTimer = MonoTime.currTime;
    }

    void cursorPosCallback(double x, double y) nothrow {
        tc.inputHandler.handleMouseMoveEvent(x, y);
    }

    void keyboardCallback(int key, int scancode, int action, int modifiers) nothrow {
        tc.inputHandler.handleKeyboardEvent(KeyboardEvent(key, scancode, action, modifiers));
    }

    void keyboardCharCallback(uint c) nothrow {
        tc.inputHandler.handleKeyboardCharEvent(c);
    }

    void mouseButtonCallback(int button, int action, int modifiers) nothrow {
        tc.inputHandler.handleMouseButtonEvent(button, action, modifiers);
    }

    extern(C) static void cursorPosCallbackS(GLFWwindow *win, double x, double y) nothrow {
        Renderer r = cast(Renderer)glfwGetWindowUserPointer(win);
        r.cursorPosCallback(x, y);
    }

    extern(C) static void mouseButtonCallbackS(GLFWwindow *win, int button, int action, int modifiers) nothrow {
        Renderer r = cast(Renderer)glfwGetWindowUserPointer(win);
        r.mouseButtonCallback(button, action, modifiers);
    }

    extern(C) static void keyboardCallbackS(GLFWwindow *win, int key, int scancode, int action, int modifiers) nothrow {
        Renderer r = cast(Renderer)glfwGetWindowUserPointer(win);
        r.keyboardCallback(key, scancode, action, modifiers);
    }

    extern(C) static void keyboardCharCallbackS(GLFWwindow *win, uint c) nothrow {
        Renderer r = cast(Renderer)glfwGetWindowUserPointer(win);
        r.keyboardCharCallback(c);
    }

    void centerWindow() {
        GLFWmonitor *monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode *mode = glfwGetVideoMode(monitor);
        int monitorX, monitorY;
        glfwGetMonitorPos(monitor, &monitorX, &monitorY);

        int windowWidth, windowHeight;
        glfwGetWindowSize(window, &windowWidth, &windowHeight);

        glfwSetWindowPos(window,
                         monitorX + (mode.width - windowWidth) / 2,
                         monitorY + (mode.height - windowHeight) / 2);
    }

    void init() {
        writeln("Initializing rendering subsystem");

        DerelictGLFW3.load();
        gfmgl = new OpenGL(new FileLogger(stderr));

        if(!glfwInit()) throw new Error("GLFW init failed");

        writeln("GLFW version: " ~ to!string(glfwGetVersionString()));

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

        window = glfwCreateWindow(800, 500, "Electory " ~ electoryVersionString, null, null);
        if(!window) throw new Error("GLFW create window failed");
        centerWindow();

        writeln("Initializing renderer->input bridge");

        glfwSetWindowUserPointer(window, cast(void*)this);
        glfwSetKeyCallback(window, &keyboardCallbackS);
        glfwSetCursorPosCallback(window, &cursorPosCallbackS);
        glfwSetMouseButtonCallback(window, &mouseButtonCallbackS);
        glfwSetCharCallback(window, &keyboardCharCallbackS);
        double mx, my;
        glfwGetCursorPos(window, &mx, &my);
        tc.inputHandler.initCursor(mx, my);

        writeln("Initializing renderer");

        glfwMakeContextCurrent(window);

        writeln("OpenGL version: " ~ to!string(gfmgl.getVersionString()));
        writeln("OpenGL vendor: " ~ to!string(gfmgl.getVendorString()));
        writeln("OpenGL renderer: " ~ to!string(gfmgl.getRendererString()));

        glfwSwapInterval(1);

        gfmgl.reload();

        shaderManager = new ShaderManager(this, tc.resourceManager);
        textureManager = new TextureManager(gfmgl, tc.resourceManager);

        projectionMatrixStack = new MatrixStack!(4,float);
        viewMatrixStack = new MatrixStack!(4,float);
        modelMatrixStack = new MatrixStack!(4,float);

        worldRenderer = new WorldRenderer(this);
        worldRenderer.init();

        writeln("Initializing GUI subsystem");

        /*guiRenderer = new GuiRenderer(this);
        guiRenderer.init();*/
    }

    bool isInLoop() @nogc nothrow  {
        return !glfwWindowShouldClose(window);
    }

    void render(float renderPartialTicks) {
        int w, h;

        glfwGetFramebufferSize(window, &w, &h);

        glViewport(0, 0, w, h);

        glClearColor(0.529f, 0.807f, 0.921f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if(worldRenderer.world != tc.world) {
            worldRenderer.setWorld(tc.world);
        }

        if(tc.world/* && !tc.guiHandler.blocksWorldInput*/) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        } else {
            //guiRenderer.updateCursors();
        }

        worldRenderer.render(renderPartialTicks);

        //guiRenderer.render();
        guiHandler.render(this);

        glfwSwapBuffers(window);

        frameCounter++;

        if(MonoTime.currTime.ticks - fpsTimer.ticks >= MonoTime.ticksPerSecond) {
            fps = frameCounter;
            frameCounter = 0;
            fpsTimer = MonoTime.currTime;
        }
    }

    ~this() {
    	destroy(shaderManager);
    	destroy(textureManager);
    	//destroy(guiRenderer);
    	destroy(projectionMatrixStack);
    	destroy(viewMatrixStack);
    	destroy(modelMatrixStack);
        destroy(worldRenderer);
    }
}
