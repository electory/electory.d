module electory.client.render.data;

import gfm.math.vector;

struct WorldVertex {
    vec3f position;
    vec2f texCoord;
    float ao = 0;
}

struct UIVertex {
    vec2f position = vec2f(0, 0);
    vec2f texCoord = vec2f(0, 0);
    vec4f color = vec4f(1, 1, 1, 1);
}