module electory.client.render.shader;

import electory.resource.manager;
import electory.client.render.render;
import gfm.opengl.shader;
import gfm.opengl.program;
import containers.hashmap;
import electory.client.game;
import std.path;
import std.file;
import derelict.opengl;
import toml;

class ShaderManager {
    private:
    GLShader[ResourceLocation] loadedShaders;
    GLProgram[ResourceLocation] loadedPrograms;
    ResourceManager resMan;
    Renderer renderer;

    public:
    this(Renderer renderer, ResourceManager resMan) {
        this.renderer = renderer;
        this.resMan = resMan;
    }

    @disable this();

    GLShader getShader(const ResourceLocation res) {
        if(res in loadedShaders) {
            return loadedShaders[res];
        }

        string path = resMan.getResourcePath(res);

        string ext = extension(res.path);

        GLenum shaderType;
        if(ext == ".fp") {
            shaderType = GL_FRAGMENT_SHADER;
        } else if(ext == ".vp") {
            shaderType = GL_VERTEX_SHADER;
        } else if(ext == ".gp") {
            shaderType = GL_GEOMETRY_SHADER;
        } else if(ext == ".cp") {
            shaderType = GL_COMPUTE_SHADER;
        } else if(ext == ".tcp") {
            shaderType = GL_TESS_CONTROL_SHADER;
        } else if(ext == ".tep") {
            shaderType = GL_TESS_EVALUATION_SHADER;
        } else {
            throw new Exception("invalid shader extension " ~ ext);
        }

        GLShader shader = new GLShader(renderer.gfmgl, shaderType);

        shader.load([readText(path)]);

        shader.compile();

        loadedShaders[res] = shader;

        return shader;
    }

    GLProgram getProgram(const ResourceLocation res) {
        if(res in loadedPrograms) {
            return loadedPrograms[res];
        }

        string path = resMan.getResourcePath(res);

        TOMLDocument doc = parseTOML(readText(path));

        TOMLValue[] attachArray = doc["attach"].array;

        GLProgram program = new GLProgram(renderer.gfmgl);

        foreach(attachLocVal; attachArray) {
            auto attachLoc = ResourceLocation(attachLocVal.str);

            program.attach(getShader(attachLoc));
        }

        program.link();

        loadedPrograms[res] = program;

        return program;
    }

    ~this() {
        foreach(ResourceLocation resLoc, GLShader shader; loadedShaders) {
            shader.destroy;
        }
        foreach(ResourceLocation resLoc, GLProgram program; loadedPrograms) {
            program.destroy;
        }
    }
}