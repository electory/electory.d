module electory.client.render.block;

import electory.world.world;
import electory.block.block;
import electory.client.render.atlas;
import electory.client.render.data;
import electory.utils.enums;
import gfm.math;

immutable interface BlockRenderer {
    void bakeInWorld(
        int x, int y, int z,
        int renderPass,
        immutable ChunkCache cache,
        immutable AtlasSpriteManager spriteManager,
        ref const Block block,
        ref WorldVertex[] verts
    ) immutable;
}

immutable class BlockRendererCube : BlockRenderer {
    private static int getAOValue(const(IBlockAccess) world, int x, int y, int z, int bx, int by, int bz, int vx, int vy, int vz) {
		assert(vx == 0 || vy == 0 || vz == 0);

		int side1 = 0;
		int side2 = 0;

		if(vx == 0) {
			side1 = world.getBlockAt(x + bx, y + by + vy, z + bz) !is null;
			side2 = world.getBlockAt(x + bx, y + by, z + bz + vz) !is null;
		} else if(vy == 0) {
			side1 = world.getBlockAt(x + bx + vx, y + by, z + bz) !is null;
			side2 = world.getBlockAt(x + bx, y + by, z + bz + vz) !is null;
		} else if(vz == 0) {
			side1 = world.getBlockAt(x + bx + vx, y + by, z + bz) !is null;
			side2 = world.getBlockAt(x + bx, y + by + vy, z + bz) !is null;
		}

		int corner = world.getBlockAt(x + bx + vx, y + by + vy, z + bz + vz) !is null;

		if(side1 && side2) {
			return 0;
		}

		return 3 - (side1 + side2 + corner);
	}

    private static void flipLastQuad(ref WorldVertex[] quads) {
	    WorldVertex v1 = quads[$-6];
	    WorldVertex v2 = quads[$-5];
	    WorldVertex v3 = quads[$-4];
	    WorldVertex v4 = quads[$-2];
	    quads[$-6] = quads[$-1] = v2;
	    quads[$-5] = v3;
	    quads[$-4] = quads[$-3] = v4;
	    quads[$-2] = v1;
    }

    override void bakeInWorld(
        int x, int y, int z,
        int renderPass,
        immutable ChunkCache cache,
        immutable AtlasSpriteManager spriteManager,
        ref const Block block,
        ref WorldVertex[] verts
    ) {
        bool shouldAO = block.shouldBeAmbientOccluded;

        if (block.shouldntFaceBeCulled(cache, x, y, z, renderPass, EnumSide.SIDE_BOTTOM)) {
            immutable AtlasSprite sprite = block.getAtlasSprite(spriteManager, EnumSide.SIDE_BOTTOM);

            immutable int ao00 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, -1, -1, -1, 0) : 3;
            immutable int ao01 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, -1, -1, +1, 0) : 3;
            immutable int ao11 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, -1, +1, +1, 0) : 3;
            immutable int ao10 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, -1, +1, -1, 0) : 3;

            verts ~= WorldVertex(vec3f(x, y, z), vec2f(sprite.getMinU(), sprite.getMinV()), ao00);
            verts ~= WorldVertex(vec3f(x, y + 1, z), vec2f(sprite.getMinU(), sprite.getMaxV()), ao01);
            verts ~= WorldVertex(vec3f(x + 1, y + 1, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao11);

            verts ~= WorldVertex(vec3f(x + 1, y + 1, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao11);
            verts ~= WorldVertex(vec3f(x + 1, y, z), vec2f(sprite.getMaxU(), sprite.getMinV()), ao10);
            verts ~= WorldVertex(vec3f(x, y, z), vec2f(sprite.getMinU(), sprite.getMinV()), ao00);

            if(ao00 + ao11 < ao01 + ao10) flipLastQuad(verts);
        }
        if (block.shouldntFaceBeCulled(cache, x, y, z, renderPass, EnumSide.SIDE_TOP)) {
            immutable AtlasSprite sprite = block.getAtlasSprite(spriteManager, EnumSide.SIDE_TOP);

            immutable int ao00 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, +1, -1, -1, 0) : 3;
            immutable int ao01 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, +1, -1, +1, 0) : 3;
            immutable int ao11 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, +1, +1, +1, 0) : 3;
            immutable int ao10 = shouldAO ? getAOValue(cache, x, y, z, 0, 0, +1, +1, -1, 0) : 3;

            verts ~= WorldVertex(vec3f(x + 1, y + 1, z + 1), vec2f(sprite.getMinU(), sprite.getMaxV()), ao11);
            verts ~= WorldVertex(vec3f(x, y + 1, z + 1), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao01);
            verts ~= WorldVertex(vec3f(x, y, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao00);

            verts ~= WorldVertex(vec3f(x, y, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao00);
            verts ~= WorldVertex(vec3f(x + 1, y, z + 1), vec2f(sprite.getMinU(), sprite.getMinV()), ao10);
            verts ~= WorldVertex(vec3f(x + 1, y + 1, z + 1), vec2f(sprite.getMinU(), sprite.getMaxV()), ao11);

            if(ao00 + ao11 < ao01 + ao10) flipLastQuad(verts);
        }
        if (block.shouldntFaceBeCulled(cache, x, y, z, renderPass, EnumSide.SIDE_SOUTH)) {
            immutable AtlasSprite sprite = block.getAtlasSprite(spriteManager, EnumSide.SIDE_SOUTH);

            immutable int ao00 = shouldAO ? getAOValue(cache, x, y, z, -1, 0, 0, 0, -1, -1) : 3;
            immutable int ao01 = shouldAO ? getAOValue(cache, x, y, z, -1, 0, 0, 0, -1, +1) : 3;
            immutable int ao11 = shouldAO ? getAOValue(cache, x, y, z, -1, 0, 0, 0, +1, +1) : 3;
            immutable int ao10 = shouldAO ? getAOValue(cache, x, y, z, -1, 0, 0, 0, +1, -1) : 3;

            verts ~= WorldVertex(vec3f(x, y, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao00);
            verts ~= WorldVertex(vec3f(x, y, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao01);
            verts ~= WorldVertex(vec3f(x, y + 1, z + 1), vec2f(sprite.getMinU(), sprite.getMinV()), ao11);

            verts ~= WorldVertex(vec3f(x, y + 1, z + 1), vec2f(sprite.getMinU(), sprite.getMinV()), ao11);
            verts ~= WorldVertex(vec3f(x, y + 1, z), vec2f(sprite.getMinU(), sprite.getMaxV()), ao10);
            verts ~= WorldVertex(vec3f(x, y, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao00);

            if(ao00 + ao11 < ao01 + ao10) flipLastQuad(verts);
        }
        if (block.shouldntFaceBeCulled(cache, x, y, z, renderPass, EnumSide.SIDE_NORTH)) {
            immutable AtlasSprite sprite = block.getAtlasSprite(spriteManager, EnumSide.SIDE_NORTH);

            immutable int ao00 = shouldAO ? getAOValue(cache, x, y, z, +1, 0, 0, 0, -1, -1) : 3;
            immutable int ao01 = shouldAO ? getAOValue(cache, x, y, z, +1, 0, 0, 0, -1, +1) : 3;
            immutable int ao11 = shouldAO ? getAOValue(cache, x, y, z, +1, 0, 0, 0, +1, +1) : 3;
            immutable int ao10 = shouldAO ? getAOValue(cache, x, y, z, +1, 0, 0, 0, +1, -1) : 3;

            verts ~= WorldVertex(vec3f(x + 1, y + 1, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao11);
            verts ~= WorldVertex(vec3f(x + 1, y, z + 1), vec2f(sprite.getMinU(), sprite.getMinV()), ao01);
            verts ~= WorldVertex(vec3f(x + 1, y, z), vec2f(sprite.getMinU(), sprite.getMaxV()), ao00);

            verts ~= WorldVertex(vec3f(x + 1, y, z), vec2f(sprite.getMinU(), sprite.getMaxV()), ao00);
            verts ~= WorldVertex(vec3f(x + 1, y + 1, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao10);
            verts ~= WorldVertex(vec3f(x + 1, y + 1, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao11);

            if(ao00 + ao11 < ao01 + ao10) flipLastQuad(verts);
        }
        if (block.shouldntFaceBeCulled(cache, x, y, z, renderPass, EnumSide.SIDE_WEST)) {
            immutable AtlasSprite sprite = block.getAtlasSprite(spriteManager, EnumSide.SIDE_WEST);

            immutable int ao00 = shouldAO ? getAOValue(cache, x, y, z, 0, -1, 0, -1, 0, -1) : 3;
            immutable int ao01 = shouldAO ? getAOValue(cache, x, y, z, 0, -1, 0, -1, 0, +1) : 3;
            immutable int ao11 = shouldAO ? getAOValue(cache, x, y, z, 0, -1, 0, +1, 0, +1) : 3;
            immutable int ao10 = shouldAO ? getAOValue(cache, x, y, z, 0, -1, 0, +1, 0, -1) : 3;

            verts ~= WorldVertex(vec3f(x + 1, y, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao11);
            verts ~= WorldVertex(vec3f(x, y, z + 1), vec2f(sprite.getMinU(), sprite.getMinV()), ao01);
            verts ~= WorldVertex(vec3f(x, y, z), vec2f(sprite.getMinU(), sprite.getMaxV()), ao00);

            verts ~= WorldVertex(vec3f(x, y, z), vec2f(sprite.getMinU(), sprite.getMaxV()), ao00);
            verts ~= WorldVertex(vec3f(x + 1, y, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao10);
            verts ~= WorldVertex(vec3f(x + 1, y, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao11);

            if(ao00 + ao11 < ao01 + ao10) flipLastQuad(verts);
        }
        if (block.shouldntFaceBeCulled(cache, x, y, z, renderPass, EnumSide.SIDE_EAST)) {
            immutable AtlasSprite sprite = block.getAtlasSprite(spriteManager, EnumSide.SIDE_EAST);

            immutable int ao00 = shouldAO ? getAOValue(cache, x, y, z, 0, +1, 0, -1, 0, -1) : 3;
            immutable int ao01 = shouldAO ? getAOValue(cache, x, y, z, 0, +1, 0, -1, 0, +1) : 3;
            immutable int ao11 = shouldAO ? getAOValue(cache, x, y, z, 0, +1, 0, +1, 0, +1) : 3;
            immutable int ao10 = shouldAO ? getAOValue(cache, x, y, z, 0, +1, 0, +1, 0, -1) : 3;

            verts ~= WorldVertex(vec3f(x, y + 1, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao00);
            verts ~= WorldVertex(vec3f(x, y + 1, z + 1), vec2f(sprite.getMaxU(), sprite.getMinV()), ao01);
            verts ~= WorldVertex(vec3f(x + 1, y + 1, z + 1), vec2f(sprite.getMinU(), sprite.getMinV()), ao11);

            verts ~= WorldVertex(vec3f(x + 1, y + 1, z + 1), vec2f(sprite.getMinU(), sprite.getMinV()), ao11);
            verts ~= WorldVertex(vec3f(x + 1, y + 1, z), vec2f(sprite.getMinU(), sprite.getMaxV()), ao10);
            verts ~= WorldVertex(vec3f(x, y + 1, z), vec2f(sprite.getMaxU(), sprite.getMaxV()), ao00);

            if(ao00 + ao11 < ao01 + ao10) flipLastQuad(verts);
        }
    }
}

enum RenderPasses {
    RENDERPASS_WORLD_BASE,
    RENDERPASS_WORLD_WATER,
    RENDERPASS_COUNT
}


immutable BlockRendererCube cubeRenderer;

static this() {
    cubeRenderer = new BlockRendererCube();
}