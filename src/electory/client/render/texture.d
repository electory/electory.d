module electory.client.render.texture;

import electory.resource.manager;
import gfm.opengl;
import containers.hashmap;
import std.path;
import std.file;
import derelict.opengl;
import toml;
import dlib.image.io.png;
import dlib.image.image;
import std.conv;

class TextureManager {
    private:
    GLTexture2D[ResourceLocation] loaded2DTextures;
    ResourceManager resMan;
    OpenGL gfmgl;

    public:
    this(OpenGL gfmgl, ResourceManager resMan) {
        this.gfmgl = gfmgl;
        this.resMan = resMan;
    }

    @disable this();

    GLTexture2D getTexture2D(const ResourceLocation res) {
        if(res in loaded2DTextures) {
            return loaded2DTextures[res];
        }

        string path = resMan.getResourcePath(res);

        auto image = loadPNG(path);
        //auto image = convert!(Image!(PixelFormat.RGBA8))(unconv_image);

        /*GLTexture2D texture = new GLTexture2D(renderer.gfmgl);

        GLenum internalformat = 0;
        GLenum format = 0;
        GLenum textype = 0;
        switch(image.pixelFormat) {
            case PixelFormat.RGBA8:
                internalformat = GL_RGBA;
                format = GL_RGBA;
                textype = GL_UNSIGNED_BYTE;
                break;
            case PixelFormat.RGB8:
                internalformat = GL_RGB;
                format = GL_RGB;
                textype = GL_UNSIGNED_BYTE;
                break;
            default:
                throw new Exception("unsupported texture format");
                break;

        }

        texture.setMagFilter(GL_NEAREST);
        texture.setMinFilter(GL_NEAREST);
        texture.setWrapS(GL_REPEAT);
        texture.setWrapT(GL_REPEAT);
        texture.setImage(0, internalformat, image.width, image.height, 0, format, textype, cast(void*)image.data);

        loaded2DTextures[res] = texture;

        return texture;*/
        return registerTexture2D(res, image);
    }

    GLTexture2D registerTexture2D(const ResourceLocation res, SuperImage image) {
        GLTexture2D texture;
        if(res in loaded2DTextures) {
            texture = loaded2DTextures[res];
        } else {
            texture = new GLTexture2D(gfmgl);
        }

        GLenum internalformat = 0;
        GLenum format = 0;
        GLenum textype = 0;
        switch(image.pixelFormat) {
            case PixelFormat.RGBA8:
                internalformat = GL_RGBA;
                format = GL_RGBA;
                textype = GL_UNSIGNED_BYTE;
                break;
            case PixelFormat.RGB8:
                internalformat = GL_RGB;
                format = GL_RGB;
                textype = GL_UNSIGNED_BYTE;
                break;
            case PixelFormat.L8:
                image = image.convert!(Image!(PixelFormat.RGBA8));
                internalformat = GL_RGBA;
                format = GL_RGBA;
                textype = GL_UNSIGNED_BYTE;
                break;
            default:
                throw new Exception("unsupported texture format " ~ to!string(image.pixelFormat));

        }

        texture.setMagFilter(GL_NEAREST);
        texture.setMinFilter(GL_NEAREST);
        texture.setWrapS(GL_REPEAT);
        texture.setWrapT(GL_REPEAT);
        texture.setImage(0, internalformat, image.width, image.height, 0, format, textype, cast(void*)image.data);

        loaded2DTextures[res] = texture;

        return texture;
    }

    GLTexture2D registerTexture2D_RGBA(const ResourceLocation res, void *data, int width, int height) {
    	GLTexture2D texture;
        if(res in loaded2DTextures) {
            texture = loaded2DTextures[res];
        } else {
            texture = new GLTexture2D(gfmgl);
        }

        texture.setMagFilter(GL_NEAREST);
        texture.setMinFilter(GL_NEAREST);
        texture.setWrapS(GL_REPEAT);
        texture.setWrapT(GL_REPEAT);
        texture.setImage(0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, cast(void*)data);

        loaded2DTextures[res] = texture;

        return texture;
    }

    ~this() {
        foreach(ResourceLocation resLoc, GLTexture2D tex; loaded2DTextures) {
            tex.destroy;
        }
    }
}