module electory.client.render.world;

import gfm.opengl.buffer;
import gfm.opengl.program;
import gfm.opengl.vertex;
import gfm.opengl.uniform;
import gfm.opengl.texture;
import gfm.opengl.vao;
import gfm.math.vector;
import gfm.math.funcs;
import derelict.opengl;
import derelict.glfw3;
import std.typecons;
import std.container;
import std.algorithm.mutation;
import std.stdio;
import std.math;
import std.concurrency;
import core.atomic;
import core.time;
import core.memory;

import electory.world.chunk;
import electory.world.chunkprovider;
import electory.world.world;
import electory.client.render.data;
import electory.client.render.render;
import electory.client.render.atlas;
import electory.resource.manager;
import electory.block.block;
import electory.utils.enums;
import electory.client.render.block;

struct ChunkBakeryTerminateEvent {}
struct ChunkBakeryExecuteEvent {
    shared WorldVertex[]* bakeTarget;
    shared bool* bakeStatusTarget;
    shared Object bakeLock;
    shared ChunkCache chunkCache;
    shared int sx, sy, sz;
    shared(AtlasSpriteManager) spriteManager;
    shared int renderPass;
}

void chunkBakeLoop() {
    static void flipLastQuad(ref WorldVertex[] quads) {
	    WorldVertex v1 = quads[$-6];
	    WorldVertex v2 = quads[$-5];
	    WorldVertex v3 = quads[$-4];
	    WorldVertex v4 = quads[$-2];
	    quads[$-6] = quads[$-1] = v2;
	    quads[$-5] = v3;
	    quads[$-4] = quads[$-3] = v4;
	    quads[$-2] = v1;
    }

    static int getAOValue(const(IBlockAccess) world, int x, int y, int z, int bx, int by, int bz, int vx, int vy, int vz) {
		assert(vx == 0 || vy == 0 || vz == 0);

		int side1 = 0;
		int side2 = 0;

		if(vx == 0) {
			side1 = world.getBlockAt(x + bx, y + by + vy, z + bz) !is null;
			side2 = world.getBlockAt(x + bx, y + by, z + bz + vz) !is null;
		} else if(vy == 0) {
			side1 = world.getBlockAt(x + bx + vx, y + by, z + bz) !is null;
			side2 = world.getBlockAt(x + bx, y + by, z + bz + vz) !is null;
		} else if(vz == 0) {
			side1 = world.getBlockAt(x + bx + vx, y + by, z + bz) !is null;
			side2 = world.getBlockAt(x + bx, y + by + vy, z + bz) !is null;
		}

		int corner = world.getBlockAt(x + bx + vx, y + by + vy, z + bz + vz) !is null;

		if(side1 && side2) {
			return 0;
		}

		return 3 - (side1 + side2 + corner);
	}

    bool doStop = false;
    while(true) {
        receive(
            (ChunkBakeryTerminateEvent e) {
                doStop = true; 
            },
            (ChunkBakeryExecuteEvent e) {
                WorldVertex[] verts;

                immutable int sx = e.sx;
                immutable int sy = e.sy;
                immutable int sz = e.sz;

                auto cache = cast(immutable)e.chunkCache;
                auto spriteManager = cast(immutable)e.spriteManager;

                for (int x = sx; x < sx + 16; x++) {
                    for (int y = sy; y < sy + 16; y++) {
                        for (int z = sz; z < sz + 16; z++) {
                            const(Block) block = cache.getBlockAt(x, y, z);
                            if (block is null) {
                                continue;
                            }
                            
                            if (!block.shouldRenderInPass(e.renderPass)) {
                                continue;
                            }

                            block.getRenderer().bakeInWorld(x, y, z, e.renderPass, cache, spriteManager, block, verts);
                        }
                    }
                }
                synchronized(e.bakeLock) {
                    *e.bakeTarget = cast(shared)verts;
                    *e.bakeStatusTarget = true;
                }
                
            }
        );
        if(doStop) break;
    }
}

class ChunkRenderer {
	private:
    vec3i position = void;

    public:

    Unique!GLBuffer[RenderPasses.RENDERPASS_COUNT] vbos;
    World world;
    bool initialized = false;
    // ulong vertexCount;
    Renderer renderer;
    GLProgram[RenderPasses.RENDERPASS_COUNT] programs;
    Unique!(GLVAO)[RenderPasses.RENDERPASS_COUNT] vaos;
    WorldRenderer worldRenderer;
    immutable(AtlasSpriteManager) spriteManager;
    bool shouldRenderUpdate = true;
    shared Object vertexDataLock = new Object;
    shared WorldVertex[][RenderPasses.RENDERPASS_COUNT] vertexDataToUpload;
    shared bool[RenderPasses.RENDERPASS_COUNT] vertexDataToUploadReadinessess;

    this(Renderer renderer, WorldRenderer worldRenderer, immutable(AtlasSpriteManager) spriteManager) {
        this.renderer = renderer;
        this.worldRenderer = worldRenderer;
        this.spriteManager = spriteManager;
    }

    void init() {
        foreach(ref program; programs) {
            program = renderer.shaderManager.getProgram(ResourceLocation("base:shaders/terrain.toml"));
        }

        foreach(ref vbo; vbos) {
            if(vbo.isEmpty()) {
                Unique!GLBuffer glBuf = new GLBuffer(renderer.gfmgl, GL_ARRAY_BUFFER, GL_DYNAMIC_DRAW);
                vbo = move(glBuf);
            }
        }

        foreach(i, ref vao; vaos) {
            if(vao.isEmpty()) {
                Unique!GLVAO glVao = new GLVAO(renderer.gfmgl);
                vao = move(glVao);
            }

            vao.bind();
            vbos[i].bind();
            auto vspec = new VertexSpecification!WorldVertex(programs[i]);
            vspec.use();
            vao.unbind();
        }

        initialized = true;
    }

    void setPosition(vec3i pos) {
        vec3i oldpos = position;
        position = pos;

        if(oldpos != pos) {
            shouldRenderUpdate = true;
        }
    }

    void addToChunkCacheList(ref RedBlackTree!ChunkPos theList) {
        foreach(x; position.x - 1..position.x + 2) {
            foreach(y; position.y - 1..position.y + 2) {
                foreach(z; position.z - 1..position.z + 2) {
                    theList.insert(ChunkPos(x, y, z));
                }   
            }   
        }
    }

    bool needsBake() {
        return shouldRenderUpdate && !shouldRenderFail;
    }

    bool shouldRenderFail() {
        return !world || !world.chunkProvider.provideChunk(position.x, position.y, position.z);
    }

    void bake(immutable ChunkCache cache) {
        shouldRenderUpdate = false;

        if(!initialized) init();

        atomicOp!"+="(worldRenderer.chunkUpdatesCounter, 1);

        /*if(!world) {
            shouldRenderUpdate = true;
            return;
        }*/

        auto chunk = world.chunkProvider.provideChunk(position.x, position.y, position.z);

        /*if(!chunk) {
            shouldRenderUpdate = true;
            return;
        }*/

        //auto cache = new immutable ChunkCache(world, chunk.chunkX - 1, chunk.chunkY - 1, chunk.chunkZ - 1, chunk.chunkX + 1, chunk.chunkY + 1, chunk.chunkZ + 1);

        immutable int sx = chunk.chunkX << 4;
        immutable int sy = chunk.chunkY << 4;
        immutable int sz = chunk.chunkZ << 4;

        foreach(i, ref dataTarget; vertexDataToUpload) {
            worldRenderer.chunkBakeThread.send(
                ChunkBakeryExecuteEvent(&dataTarget, 
                                        &vertexDataToUploadReadinessess[i],
                                        vertexDataLock,
                                        cast(shared)cache,
                                        cast(shared)sx,
                                        cast(shared)sy,
                                        cast(shared)sz,
                                        cast(shared)spriteManager,
                                        cast(shared int)i
                                        )
            );
        }
    }

    void render(int i) {
        if(shouldRenderFail) return;

        if(!initialized) init();

        //foreach(i; 0..cast(int)RenderPasses.RENDERPASS_COUNT) {
        programs[i].use();
        programs[i].uniform("projectionMatrix").set(renderer.projectionMatrixStack.top);
        programs[i].uniform("viewMatrix").set(renderer.viewMatrixStack.top);
        programs[i].uniform("modelMatrix").set(renderer.modelMatrixStack.top);

        programs[i].uniform("texture").set(0);

        synchronized(vertexDataLock) {
            if(vertexDataToUploadReadinessess[i]) {
                WorldVertex[] data = cast(WorldVertex[])vertexDataToUpload[i];
                vbos[i].setData(data);
                vertexDataToUpload[i] = null;
                vertexDataToUploadReadinessess[i] = false;
                data.destroy!false;
                GC.free(cast(void*)data);
            }
        }

        vaos[i].bind();
        glDrawArrays(GL_TRIANGLES, 0, cast(uint)(vbos[i].size / WorldVertex.sizeof));
        vaos[i].unbind();
        //}
    }
}

/**
 * Renders the world.
 */
class WorldRenderer {
    public:
    /// The world object
    World world;

    /// The root renderer object
    Renderer renderer;

    /// The block sprite manager
    AtlasSpriteManager blockSpriteManager;

    shared int chunkUpdatesCounter = 0;
    MonoTime chunkUpdatesTimer = void;
    ChunkRenderer[][][] chunkRenderers;
    int renderDistance = 8;
    int renderDistanceX2 = 16;
    Tid chunkBakeThread;

    this(Renderer renderer) {
        this.renderer = renderer;
        this.blockSpriteManager = new AtlasSpriteManagerImplStitch(renderer.tc.resourceManager, renderer.textureManager, ResourceLocation("base:textures/terrain.png"));
        chunkUpdatesTimer = MonoTime.currTime;
        chunkBakeThread = spawn(&chunkBakeLoop);
    }

    void setWorld(World world) {
        this.world = world;
        foreach(x; 0..renderDistanceX2) {
	        foreach(y; 0..renderDistanceX2) {
		        foreach(z; 0..renderDistanceX2) {
                    chunkRenderers[x][y][z].world = world;
                }
            }
        }
    }

    void init() {
        foreach(block; renderer.tc.blockRegistry.getRegisteredBlocks) {
            block.registerAtlasSprites(blockSpriteManager);
        }
        blockSpriteManager.buildAtlas();

        chunkRenderers = new ChunkRenderer[][][](renderDistanceX2, renderDistanceX2, renderDistanceX2);

        foreach(x; 0..renderDistanceX2) {
	        foreach(y; 0..renderDistanceX2) {
		        foreach(z; 0..renderDistanceX2) {
		        	chunkRenderers[x][y][z] = new ChunkRenderer(renderer, this,
                                                                new immutable ImmutableAtlasSpriteManagerImpl(
                                                                    cast(AtlasSpriteManagerImplStitch)blockSpriteManager
                                                                ));
                    chunkRenderers[x][y][z].world = world;
		        }
	        }
        }
    }

    void updateChunkRendererPositions() {
        foreach(x; world.renderCenter.x - renderDistance .. world.renderCenter.x + renderDistance) {
	        foreach(y; world.renderCenter.y - renderDistance .. world.renderCenter.y + renderDistance) {
		        foreach(z; world.renderCenter.z - renderDistance .. world.renderCenter.z + renderDistance) {
                    chunkRenderers[((x % renderDistanceX2) + renderDistanceX2) % renderDistanceX2]
                                  [((y % renderDistanceX2) + renderDistanceX2) % renderDistanceX2]
                                  [((z % renderDistanceX2) + renderDistanceX2) % renderDistanceX2]
                                  .setPosition(vec3i(x, y, z));
                }
            }
        }
    }

    void renderPrePass(int pass, vec3f eyePos) {
        if(pass == RenderPasses.RENDERPASS_WORLD_WATER) {
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glEnable(GL_BLEND);
            glDepthMask(GL_FALSE);
            auto eyePosBlockPos = vec3i(cast(int)floor(eyePos.x), cast(int)floor(eyePos.y), cast(int)floor(eyePos.z));
            auto eyeBlock = world.getBlockAt(eyePosBlockPos.x, eyePosBlockPos.y, eyePosBlockPos.z);
            if(eyeBlock && eyeBlock.shouldRenderInPass(pass)) {
                glCullFace(GL_FRONT);   
            }
        }
    }

    void renderPostPass(int pass, vec3f eyePos) {
        if(pass == RenderPasses.RENDERPASS_WORLD_WATER) {
            glDisable(GL_BLEND);
            glDepthMask(GL_TRUE);
            glCullFace(GL_BACK);
        }
    }

    /***
     * Render the world
     * Params:
     *      renderPartialTicks = partial ticks for renderer
     */
    void render(float renderPartialTicks) {
        if(!world) return;

        if(MonoTime.currTime.ticks - chunkUpdatesTimer.ticks >= MonoTime.ticksPerSecond) {
        	chunkUpdatesCounter = 0;
        	chunkUpdatesTimer = MonoTime.currTime;
        }

        int w, h;

        glfwGetFramebufferSize(renderer.window, &w, &h);

        renderer.projectionMatrixStack.push();
        renderer.viewMatrixStack.push();

        glEnable(GL_DEPTH_TEST);

        renderer.projectionMatrixStack.perspective(radians(renderer.tc.gameConfig.fov), cast(float)(w) / cast(float)(h), 0.1f, 1000.0f);
        renderer.viewMatrixStack.lookAt(vec3f(0.0f, 0.0f, 0.0f), vec3f(1.0f, 0.0f, 0.0f), vec3f(0.0f, 0.0f, 1.0f));

        vec3f playerPos = world.currentPlayer.getInterpolatedPosition(renderPartialTicks);

        renderer.viewMatrixStack.rotate(world.currentPlayer.pitch, vec3f(0.0f, 1.0f, 0.0f));
        renderer.viewMatrixStack.rotate(world.currentPlayer.yaw, vec3f(0.0f, 0.0f, 1.0f));
        auto eyePos = playerPos + vec3f(0.0f, 0.0f, 1.5f);
        renderer.viewMatrixStack.translate(-eyePos);

        glEnable(GL_CULL_FACE);

        updateChunkRendererPositions();

        RedBlackTree!ChunkPos chunkCachePreList = new RedBlackTree!ChunkPos;

        foreach(a; chunkRenderers) {
            foreach(b; a) {
                foreach(cRenderer; b) {
                    if(auto chunk = world.getChunkAt(cRenderer.position.x, cRenderer.position.y, cRenderer.position.z)) {
                        if(chunk.flags & ChunkFlags.NEEDS_RENDER_UPDATE) {
                            cRenderer.shouldRenderUpdate = true;
                            chunk.flags &= ~ChunkFlags.NEEDS_RENDER_UPDATE;
                        }
                    }
                    if(cRenderer.needsBake) {
                        cRenderer.addToChunkCacheList(chunkCachePreList);
                    }
                }
            }
        }

        if(!chunkCachePreList.empty) {
            auto cc = new immutable ChunkCache(world, chunkCachePreList);

            foreach(a; chunkRenderers) {
                foreach(b; a) {
                    foreach(cRenderer; b) {
                        if(cRenderer.needsBake) {
                            cRenderer.bake(cc);
                        }
                    }
                }
            }
        }

        GLTexture2D tex = renderer.textureManager.getTexture2D(ResourceLocation("base:textures/terrain.png"));
        tex.use(0);

        foreach(i; 0..cast(int)RenderPasses.RENDERPASS_COUNT) {
            renderPrePass(i, eyePos);
            foreach(a; chunkRenderers) {
                foreach(b; a) {
                    foreach(cRenderer; b) {
                        renderer.modelMatrixStack.push();
                        cRenderer.render(i);
                        renderer.modelMatrixStack.pop();
                    }
                }
            }
            renderPostPass(i, eyePos);
        }
        
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);

        renderer.projectionMatrixStack.pop();
        renderer.viewMatrixStack.pop();
    }

    /**
     * Marks chunk for render update
     */
    void markChunkForRenderUpdate(int x, int y, int z) {
        if(auto renderer = chunkRenderers[((x % renderDistanceX2) + renderDistanceX2) % renderDistanceX2]
                                         [((y % renderDistanceX2) + renderDistanceX2) % renderDistanceX2]
                                         [((z % renderDistanceX2) + renderDistanceX2) % renderDistanceX2]) {
            renderer.shouldRenderUpdate = true;
        }
    }

    ~this() {
        foreach(x; 0..renderDistanceX2) {
	        foreach(y; 0..renderDistanceX2) {
		        foreach(z; 0..renderDistanceX2) {
                    chunkRenderers[x][y][z].destroy!false;
                }
            }
        }
        chunkBakeThread.send(ChunkBakeryTerminateEvent());
    }
}