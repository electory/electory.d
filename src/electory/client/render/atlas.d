module electory.client.render.atlas;

import std.typecons;
import electory.resource.manager;
import electory.client.render.texture;
import containers.hashset;
import dlib.image;
import std.math;
import std.stdio;
import std.algorithm.iteration;
import std.array;

/**
 * Used as an identifier for atlas sprites
 */
struct AtlasSpriteId {
    /// The real identifier
    uint id;

    alias id this;
}

interface AtlasSprite {
    float getMinU() const;

    float getMaxU() const;

    float getMinV() const;

    float getMaxV() const;
}

interface AtlasSpriteManager {
    public:
    AtlasSpriteId registerSprite(ResourceLocation res);
    void buildAtlas();
    immutable immutable(AtlasSprite) getSpriteById(AtlasSpriteId id);
    AtlasSprite getSpriteById(AtlasSpriteId id);
}

class ImmutableAtlasSprite : immutable(AtlasSprite) {
    private:
    float minU, maxU;
    float minV, maxV;

    public:
    @disable this();

    this(float minU, float maxU, float minV, float maxV) immutable {
        this.minU = minU;
        this.maxU = maxU;
        this.minV = minV;
        this.maxV = maxV;
    }

    this(AtlasSprite sprite) immutable {
        this.minU = sprite.getMinU();
        this.maxU = sprite.getMaxU();
        this.minV = sprite.getMinV();
        this.maxV = sprite.getMaxV();
    }

    override float getMinU() const {
        return minU;
    }

    override float getMaxU() const {
        return maxU;
    }

    override float getMinV() const {
        return minV;
    }

    override float getMaxV() const {
        return maxV;
    }
}

class StitchedAtlasSprite : AtlasSprite {
    private:
    int x = void;
    int y = void;
    int side = void;
    int atlasWidth = void;
    int atlasHeight = void;
    public:

    override float getMinU() const {
        return x / cast(float)(atlasWidth);
    }

    override float getMaxU() const {
        return (x + side) / cast(float)(atlasWidth);
    }

    override float getMinV() const {
        return (y) / cast(float)(atlasHeight);
    }

    override float getMaxV() const {
        return (y + side) / cast(float)(atlasHeight);
    }
}

private struct StitcherTile {
    SuperImage image;
    StitchedAtlasSprite sprite;
    int x, y;
    int side;
    ResourceLocation res;
}

immutable class ImmutableAtlasSpriteManagerImpl : immutable(WhiteHole!AtlasSpriteManager) {
    private:
    immutable ImmutableAtlasSprite[] spritesByID;

    public:
    @disable this();

    this(AtlasSpriteManagerImplStitch parent) const {
        this.spritesByID = parent.spritesByID.map!(sprite => new immutable ImmutableAtlasSprite(sprite)).array;
    }

    override immutable(AtlasSprite) getSpriteById(AtlasSpriteId id) const {
        return spritesByID[id];
    }
}

class AtlasSpriteManagerImplStitch : AtlasSpriteManager {
    private:
    StitchedAtlasSprite[ResourceLocation] sprites;
    StitchedAtlasSprite[] spritesByID;
    int[StitchedAtlasSprite] idBySprites;

    ResourceLocation stitchTarget;
    ResourceManager resMan;
    TextureManager texMan;

    public:

    @disable this();

    this(ResourceManager resMan, TextureManager texMan, ResourceLocation stitchTarget) {
        this.stitchTarget = stitchTarget;
        this.resMan = resMan;
        this.texMan = texMan;
    }

    override AtlasSpriteId registerSprite(ResourceLocation res) {
        if(res in sprites) {
            return AtlasSpriteId(idBySprites[sprites[res]]);
        }
        auto sprite = new StitchedAtlasSprite;
        sprites[res] = sprite;
        spritesByID ~= sprite;
        auto id = idBySprites[sprite] = cast(uint)(spritesByID.length - 1);
        return AtlasSpriteId(id);
    }

    override void buildAtlas() {

        int minPotSide = 9999;
        int totalArea = 0;
        StitcherTile[][] tileGroups = [];

        foreach(res, sprite; sprites) {
            string f = resMan.getResourcePath(res);

            auto img = loadPNG(f);

            if(img.width != img.height) {
                throw new Exception("Could not stitch image " ~ res.toString() ~ " because its width is not equal to height");
            }
            if (!img.width || (img.width & (img.width - 1))) {
                throw new Exception("Could not stitch image " ~ res.toString() ~ " because its side is NPOT");
            }

            int potSide = 0;

            while((1 << potSide) < img.width) {
                potSide++;
            }

            if(potSide >= tileGroups.length) {
                tileGroups.length = potSide + 1;
            }

            totalArea += img.width * img.width;

            tileGroups[potSide] ~= StitcherTile(img, sprite, 0, 0, img.width, res);

            if(potSide < minPotSide) minPotSide = potSide;
        }


        int atlasSide = cast(int)pow(2, ceil(log(sqrt(cast(real)totalArea))/log(2)));

        immutable int largestTileSide = 1 << (tileGroups.length - 1);

        int nn = atlasSide / largestTileSide;

        bool[] slots = new bool[nn * nn];

        int firstFreeSlot = 0;

        foreach_reverse(g, tiles; tileGroups) {
            if(g < minPotSide) {
                break;
            }

            foreach(ref tile; tiles) {
                int tileSlot = firstFreeSlot;
                tile.x = tileSlot % nn * tile.side;
                tile.y = tileSlot / nn * tile.side;
                slots[tileSlot] = true;
                foreach(i, slot; slots[(tileSlot + 1)..$]) {
                    if(!slot) {
                        firstFreeSlot = cast(int)((tileSlot + 1 + i));
                        break;
                    }
                }
            }

            bool[] newSlots = new bool[slots.length * 4];

            foreach(i, oldSlot; slots) {
                int slotX = cast(int)(i % nn * 2);
                int slotY = cast(int)(i / nn * 2);

                int newNN = nn * 2;

                newSlots[slotX + slotY * newNN] = oldSlot;
                newSlots[slotX + 1 + slotY * newNN] = oldSlot;
                newSlots[slotX + (slotY + 1) * newNN] = oldSlot;
                newSlots[slotX + 1 + (slotY + 1) * newNN] = oldSlot;
            }

            int ffslotX = cast(int)(firstFreeSlot % nn * 2);
            int ffslotY = cast(int)(firstFreeSlot / nn * 2);

            nn *= 2;

            firstFreeSlot = ffslotX + ffslotY * nn;

            slots = newSlots;
        }

        Image!(PixelFormat.RGBA8) outImg = new Image!(PixelFormat.RGBA8)(atlasSide, atlasSide);

        foreach_reverse(tiles; tileGroups) {
            foreach(tile; tiles) {
                if(tile.image.channels == 2 || tile.image.channels == 4) {
                    foreach(ref p, uint x, uint y; outImg.region(tile.x, tile.y, tile.side, tile.side)) { // TODO: optimize
                        Color4f c4 = tile.image[x, y];
                        p = c4;
                    }
                } else {
                    foreach(ref p, uint x, uint y; outImg.region(tile.x, tile.y, tile.side, tile.side)) { // TODO: optimize
                        Color4f c4 = tile.image[x, y];
                        c4.a = 1.0f;
                        p = c4;
                    }
                }
                tile.sprite.x = tile.x;
                tile.sprite.y = tile.y;
                tile.sprite.side = tile.side;
                tile.sprite.atlasWidth = atlasSide;
                tile.sprite.atlasHeight = atlasSide;
            }
        }

        texMan.registerTexture2D(stitchTarget, outImg);

        writefln!"Stitched %dx%d atlas '%s'"(atlasSide, atlasSide, stitchTarget.toString());
    }

    override immutable(AtlasSprite) getSpriteById(AtlasSpriteId id) const {
        assert(false, "Immutable sprite from mutable sprite manager is bad");
    }

    override AtlasSprite getSpriteById(AtlasSpriteId id) {
        return spritesByID[id];
    }
}